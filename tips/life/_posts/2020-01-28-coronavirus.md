---
layout: base
title: 코로나 바이러스 (coronavirus)
image: /assets/img/blog/corona.png
description: >
  코로나 바이러스에 대해서 알아보자.
author: navy apple
comments: true
toc: true
permalink: /tips/life/corona
tags:
  - coronavirus
  - 코로나 바이러스
  - 신종 코로나
---

## 코로나 바이러스란?

날씨가 풀리는가 싶더니, 바이러스가 퍼지고 있습니다.<br/>
코로나 바이러스는 `중국 우한`시에서 시작됐다고 합니다.

![image](https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Coronaviruses_004_lores.jpg/220px-Coronaviruses_004_lores.jpg)

<small>출처: https://en.wikipedia.org/wiki/Coronavirus</small>

과학자들에 따르면 `코로나` 바이러스는 큰 RNA 바이러스 집단에 속하며, 총 6가지 형태로 존재하는 것으로 보고되고 있습니다.
코로나바이러스는 일반 감기(rhinovirus)뿐만 아니라 중증급성호흡기증후군(SARS) 및 중동호흡기증후군(MERS)과 같은 심각한 질병도 일으킬 수 있고, 
의료진은 바이러스를 직접적으로 치료할 수 없다라고 말했습니다.

코로나 바이러스는 인간보다는 다른 동물을 대상으로 전염되며, 종종 인간에게 전염되는 변종이 등장하여 골칫거리를 만들고 있는데 
보통은 그냥 일반 감기일 뿐이고, 심지어 별 증상이 없는 경우도 있다. 면역계가 취약한 사람이 아니라면 별 증상 없이 기침만 나므로 잠깐 이슈가 되었다가 후속 연구나 뉴스 없이 묻혀버렸다.

감기 기운이 있는 것 같을 때는 피곤한 것인 경우가 대부분이긴 하나, 알고 보니 코로나 바이러스가 일으킨 감기였던 경우 심심치 않게 발견된다.
다만 코로나 바이러스 감염이라면 소화기 이상이 동반된다.
예를 들어 감기 기운이 있는 동시에 위장과 장의 컨디션이 좋지 않은 경우, 코로나 바이러스를 의심해 보도록 하자.

![image](https://cdn.clipart.email/183eb5e49db4f242ca1dba2dcbd3d86c_washing-hands-clip-art-at-clkercom-vector-clip-art-online-_468-598.png) {: height=800}

### 손을 씻고 다닙시다.
