---
layout: base
title: 맥에서 경로 입력해서 폴더로 이동하기 (mac finder path to folder)
image: /assets/img/blog/mac-finder.png
description: >
  맥에서 경로 입력해서 폴더로 이동하기.
author: navy apple
comments: true
toc: true
permalink: /tips/life/mac-finder-path
tags:
  - macOS
  - finder
  - path
  - 파인더 경로
  - 폴더 경로
---

## 맥에서 경로 입력해서 폴더로 이동하기 (mac finder path to folder)

가끔 파일을 웹에서 다운로드했는데 이 다운받은 파일의 경로와 파일의 이름도 알고 있습니다.
하지만 경로가 너무 길다거나 하는 경우가 복잡한 경우 경로를 복사하여 붙여넣고 폴더 이동을 합니다. 

예를 들어, 윈도우의 경우 탐색기창의 경로를 입력하는 공간이 상단에 있습니다.
Windows와는 달리 맥에서는 경로를 입력하는 창이 노출되어 있지 않아 불편하게 생각 될 수 있습니다.

하지만, 사실 macOS의 파인더(finder) 광장히 잘 만들어진 앱입니다. 여러가지 기능들이 숨어있지만 이번에는 간단하게
맥 파인더(finder)에서 경로입력으로 폴더 경로 이동하는 법에 대해서 알아보겠습니다.

### 파인더(finder) 열기

![spot](/assets/img/blog/finder-path-1.png)

### `⌘` + `shift` + `g` 입력 및 path 입력하기

![finder](/assets/img/blog/finder-path-2.png)

간단하게 맥에서 경로입력으로 폴더 경로 이동하는 법을 알아보았습니다.

감사합니다.
