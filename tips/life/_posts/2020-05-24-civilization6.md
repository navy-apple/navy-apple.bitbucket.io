---
layout: base
title: 에픽게임즈 문명6 무료 배포중
image: /assets/img/blog/civilization6-1.png
description: >
  5월 22일부터 29일까지 에픽게임즈 스토어에서 문명6 무료로 다운 가능합니다.
author: 치디우기
comments: true
toc: true
permalink: /tips/life/civilization6
tags:
  - 문명
  - 문명6
  - 에픽게임즈
  - 에픽메가세일
---

요즘 코로나19로 밖에 나가는게 걱정되고, 불안하지만 집에만 있긴 따분한 분들을 위해 좋은 소식이 있어서 공유하려고 합니다. 그 소식은 바로 에픽게임즈에서 지난 5월 22일부터 `문명6`를 무료로 배포하고 있습니다. 지난주 GTA5부터 해서 매주 엄청난 게임을 무료로 배포하는 중인데 집에서 심심하셨던 분들에게 좋은 기회라고 생각합니다.

![civilization6-1](/assets/img/blog/civilization6-1.png)

저 역시 이전 문명5를 해보면서 문명 시리즈에 빠지게 되었는데 이번 기회에 `문명6`를 무료로 즐길 수 있게 되었네요

<hr>

{% assign title = 'Epic Games Store | Official Site' %}
{% assign desc = 'A curated digital storefront for PC and Mac, designed with both players and creators in mind.' %}


### 문명6 무료 다운로드 방법

{% include base/components/link-box-custom-url.html title=title description=desc url='epicgames.com' internal_url='https://www.epicgames.com/store/ko/' bg_img_url='/assets/img/blog/epicgames-link.png' %}

{% include base/components/google/infeed-ad.html slot=site.data.ad.first.infeed %}

위 링크를 통해서 `에픽게임즈` 스토어에 방문하실 수 있습니다.

![civilization6-2](/assets/img/blog/civilization6-2.png)

이미 회원이시라면 바로 다운로드를 받으실 수 있으나, 아니시라면 회원가입을 해주셔야합니다.

#### 1. 문명6 다운로드 페이지 방문하기

![civilization6-3](/assets/img/blog/civilization6-3.png)

위 그림과 같이 `찾아보기` > 문명6 카드를 클릭하시면 됩니다. 그림에도 나와있지만 `66,000`원 짜리를 무료로 다운로드 받으실 수 있습니다.

![civilization6-4](/assets/img/blog/civilization6-4.png)

또 다른 접근 방법은 새 소식을 클릭하면 위와 같이 커다란 `문명6` 카드를 확인하실 수 있고, 이 카드를 클릭해도 무료로 다운 받을 수 있는 페이지로 이동됩니다.

#### 2. 문명 다운로드 받기

![civilization6-5](/assets/img/blog/civilization6-5.png)

위 화면은 1번에서 카드를 클릭했을 때, 이동되는 페이지 입니다. 하단의 받기 버튼을 클릭하여 무료로 다운로드 받으 실 수 있습니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.second.infeed %}

<hr>

### 문명6 플레이 방법

문명6를 플레이하기 위해서는 에픽게임즈의 런처가 필요합니다.

#### 에픽게임즈 런처 다운로드

![civilization6-6](/assets/img/blog/civilization6-6.png)

 에픽게임즈 홈페이지에서 `에픽게임즈 런처`를 다운 받으신 후, 라이브러리에서 문명6를 클릭 후 설치 하실 수 있습니다.

참고로 설치를 하실 때, 설치 위치와 바로가기 설정을 할 수 있습니다.

<hr>

### 맺음

![civilization6-7](/assets/img/blog/civilization6-7.png)

현재 문명 6는 5월 29일까지 무료로 배포 중이며 이외에도 6개의 문명과 `시나리오 dlc`가 추가되는 플레티넘 에디션도 50% 세일중이라 평소 문명6에 관심있던 분이라면 좋은 기회라고 생각됩니다.

![civilization6-8](/assets/img/blog/civilization6-8.png)

추가로 에픽게임즈 스토어에서 에픽메가세일 이라고 GTA5, ANNO1860, Red Dead Redemption 2 등 여러 꿀잼 게임들을 할인 행사 중이니 코로나로 집에만 있는 요즘, 집에서 게임으로 스트레스를 해소할 수 있을 것 같습니다.

감사합니다.
