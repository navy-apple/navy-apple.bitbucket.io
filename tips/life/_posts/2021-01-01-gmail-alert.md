---
layout: base
title: "[chrome/크롬] PC에서 지메일(Gmail) 알림 설정 하기"
image: /assets/img/blog/gmail.png
description: >
  chrome에서 지메일(Gmail) 알림 설정하는 방법에 대해서 소개해드립니다.
author: navy apple
comments: true
toc: true
permalink: /tips/life/gmail-alert
tags:
  - gmail
  - 지메일
  - gmail alert
  - 지메일 알림
  - Gmail 알림
---

## [chrome/크롬] PC에서 지메일(Gmail) 알림 설정 하기

요즘에는 다양한 플랫폼에서 제공하는 메일들을 사용하는 분들이 많으실거라고 생각합니다. 저만해도 적어도 3 ~ 4 곳의 플랫폼에서 제공하는 메일 기능을 이용하고 있습니다. 그중 사용 빈도가 가장 높은 메일 서비스는 아무래도 **Gmail** 입니다. 하지만 PC에서 Gmail을 사용하다보면 불편한 점이 간혹있는데요. 그중 가장 불편했던건 아무래도 메일에 대한 알림이 자동으로 설정이 되지 않는다는 점인데요.

아웃룩같은 경우 OS에서 지원하는 어플리케이션이 따로 있어서 앱을 설치하면 기본적으로 알림에 대한 설정을 할 수 있도록 해줍니다. 하지만 웹기반의 메일 대표적으로 Gmail 같은 경우 알림설정하는 것을 직접 설정으로 들어가서 해줘야합니다.

오늘 소개해드릴 것은 Gmail 알림 설정하는 방법입니다.

> 지메일 알림 설정 확인사항 3가지
> - [Gmail 서비스에서 알림 설정](#gmail-서비스에서-알림-설정)
> - [PC 또는 노트북에서 크롬 알림 활성화](#pc-또는-노트북에서-크롬-알림-활성화)
> - [크롬/chrome에서 알림 설정](#크롬chrome에서-알림-설정)

<hr/>

### Gmail 서비스에서 알림 설정

![gmail-alert1](/assets/img/blog/gmail-alert1.png)

Gmail에 로그인하여 들어가게 되면 위 그림과 같이 오른쪽 상단에 설정 버튼이 있습니다. 이 버튼을 클릭해줍니다.

![gmail-alert2](/assets/img/blog/gmail-alert2.png)

클릭을 하면, UI 설정, 테마 설정 부분이 있지만 저희는 알림에 대한 설정을 할것이기 때문에 `모든 설정 보기`를 클릭하여 설정화면으로 진입하겠습니다.

![gmail-alert3](/assets/img/blog/gmail-alert3.png)

설정을 위한 탭들이 상당히 많지만 알림의 경우는 **기본설정**에서 설정할 수 있습니다.

![gmail-alert4](/assets/img/blog/gmail-alert4.png)

이후, 아래로 스크롤을 내리다보면 **데스크톱 알림**이라는 설정영역을 보실 수 있습니다. 이 영역에서 `새 메일 알림 사용`으로 선택해줍니다.

![gmail-alert5](/assets/img/blog/gmail-alert5.png)

그리고 제일 하단으로 스크롤 다운을 하신 후, 꼭 변경사항을 저장해주세요.

{% include base/components/google/infeed-ad.html slot=site.data.ad.first.infeed %}

### PC 또는 노트북에서 크롬 알림 활성화

저 같은 경우, 맥을 사용하는 유저이기 때문에 노트북 자체에서 크롬의 알림을 꺼놓았는지 확인이 필요합니다.

![gmail-alert6](/assets/img/blog/gmail-alert6.png)

**System Preferences**앱을 실행해주시고, 알림(Notifications) 설정을 클릭해줍니다.

![gmail-alert7](/assets/img/blog/gmail-alert7.png)

왼쪽에는 앱 리스트를 볼 수 있는데, 크롬을 선택해주고, 토글 버튼을 눌러 알림을 허용해주세요.

### 크롬/chrome에서 알림 설정

기본적으로 크롬 다운로드 후 첫 실행 시, 알림을 설정할 수 있도록 해주는 지는 기억이 나지 않지만 저는 따로 알림을 꺼놨던 기억이 있는 것 같아서 재설정을 해줄 필요가 있었습니다.

![chrome-set-noti1](/assets/img/blog/chrome-set-noti1.png)

크롬 브라우저 오른쪽 상단 버튼을 눌러 `Settings`를 클릭하여 설정화면으로 진입합니다.

![chrome-set-noti2](/assets/img/blog/chrome-set-noti2.png)

설정 검색창에 notification 또는 알림을 검색합니다. 이후 **Site Settings**로 들어갑니다.

![chrome-set-noti3](/assets/img/blog/chrome-set-noti3.png)

이후 하단으로 스크롤을 내리다보면 `Notifications`라는 탭으로 들어가실 수 있습니다.

![chrome-set-noti4](/assets/img/blog/chrome-set-noti4.png)

제가 처음 들어갔을 때는 다음과 같은 영역이 있었습니다.

- Notifications (토글)
- Block
- Allow

보시면 알 수 있겠지만, 설명을 드리자면 Notifications의 토글을 통해서 전체 알림을 켜거나 끌 수 있습니다.

만약에 꺼저 있는 상태라면 하단의 `Block`과 `Allow`를 통해서 특정 도메인에 대해서 차단하거나 허용할 수 있습니다.

Allow에 **Add** 버튼을 클릭하여 도메인을 추가해줍니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.second.infeed %}

![chrome-set-noti5](/assets/img/blog/chrome-set-noti5.png)

사이트 추가 창에서 `mail.google.com`을 입력해주고 추가 버튼을 누릅니다.

![chrome-set-noti6](/assets/img/blog/chrome-set-noti6.png)

이후, 추가된 것을 확인하실 수 있습니다.

![gmail-alert8](/assets/img/blog/gmail-alert8.png)

마지막으로 알림이 잘 오는지 테스트 해봤습니다. 정상적으로 작동하는 것을 볼 수 있습니다.

### 맺음

크롬에서 지메일 알림을 설정하는 방법에 대해서 알아보았습니다. 혹시 궁굼하신 점이나 이상한 점이 있으시다면 댓글 부탁드리겠습니다.

감사합니다.
