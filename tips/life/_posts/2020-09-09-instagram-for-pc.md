---
layout: base
title: "[chrome/크롬] 인스타그램(instagram) PC 버전 이용 방법"
image: /assets/img/blog/instagram.png
description: >
  chrome 확장 프로그램을 이용하여 PC에서 인스타그램을 사용하는 방법에 대해서 알아봅니다.
author: navy apple
comments: true
toc: true
permalink: /tips/life/instagram-for-pc
tags:
  - instagram
  - 인스타그램
  - 인스타 PC 버전
  - instagram for pc
  - 업로드
  - DM 보내기
---

## [chrome/크롬] 인스타그램(instagram) PC 버전 이용 방법

PC 상에서 넓은 화면으로 **인스타그램**을 이용하고 싶은 경우가 있습니다. 웹에서 인스타를 사용하는 경우, 사진 업로드나 DM (Direct Message)를 이용을 할 수 없습니다. 그렇기 때문에 PC 내에 있는 사진을 업로드 하기 위해서는 폰으로 사진을 옮긴 후, 업로드해야합니다.

또한 웹에서 DM을 보낼 수 없기 때문에 PC를 주로 이용하는 사용자에게는 불편을 줄 수 있습니다. 그래서 오늘은 **인스타그램 PC 버전**을 다운받고 사용하는 방법에 대해서 소개해드리도록 하겠습니다.

<hr/>

### 인스타그램 PC 버전 - 크롬 확장 프로그램

인스타그램 PC 버전은 크롬 확장 프로그램을 통해서 설치하실 수 있습니다. 확장 프로그램이기 때문에 웹의 세션과 쿠키를 이용하는 것으로 보입니다. 따라서 만약 사용하시는 크롬 브라우저에서 페이스북이나 인스타그램에 로그인이 되어있다면 확장프로그램과 연동이 되는 것을 확인하실 수 있습니다.

> 다운로드 방법

![instagram-down1](/assets/img/blog/instagram-down1.png)

구글 검색창에 `크롬 웹스토어`를 검색합니다.

![instagram-down2](/assets/img/blog/instagram-down2.png)

크롬 웹스토어 검색창에 다음과 같이 `instagram`을 검색해줍니다.

![instagram-down3](/assets/img/blog/instagram-down3.png)
![instagram-down4](/assets/img/blog/instagram-down4.png)

빨간색 박스 안의 `chrome에 추가`버튼을 클릭하면, 잠시 후 설치가 완료됩니다.

![instagram-down5](/assets/img/blog/instagram-down5.png)

설치가 완료되면, 브라우저 오른쪽 상단에 인스타그램 확장 프로그램을 클릭하면 앱을 실행하실 수 있습니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.first.infeed %}

### 실행 및 로그인

![instagram-run1](/assets/img/blog/instagram-run1.png)

저의 경우 웹으로 페이스북에 테스트 계정으로 로그인이 되어있는 상태였기 때문에 위와 같이 페이스북으로 로그인할 수 있는 버튼이 보입니다.

![instagram-run2](/assets/img/blog/instagram-run2.png)

이후 계정을 정보를 세팅해주시고 `다음` 버튼을 누르다보면, **Instagram에 오신 것을 환영합니다**라는 문구를 보실 수 있습니다.

### 사진 업로드

![instagram-upload1](/assets/img/blog/instagram-upload1.png)

사진을 업로드하는 방법은 앱과 동일합니다. 상단의 카메라 버튼과 하단의 추가 버튼을 이용하여 업로드를 할 수 있습니다.

두 버튼을 눌렀을 모두 finder 앱이 실행되고, 사진 파일을 선택할 수 있습니다. 하지만 카메라 버튼을 눌러서 업로드 할 경우, 업로드가 안됐습니다.

하단의 **추가 버튼**을 이용하시면 될 것 같습니다.

![instagram-upload2](/assets/img/blog/instagram-upload2.png)

사진을 업로드 할 수 있는 finder 창이 활성화된 것을 확인할 수 있습니다.

**주의!** 사진 파일은 jpg 타입만 업로드 할 수 있습니다.

![instagram-upload3](/assets/img/blog/instagram-upload3.png)

추가 버튼으로 사진을 업로드하고 다음 버튼을 누르고, 공유가 완료되기 기다립니다.

![instagram-upload4](/assets/img/blog/instagram-upload4.png)

정상적으로 업로드된 것을 확인하실 수 있습니다. 또한 앱과 동일하게 오른쪽 `...` 버튼을 클릭하여, 여러가지 기능을 사용할 수 있습니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.second.infeed %}

### DM (Direct Message)

![instagram-dm1](/assets/img/blog/instagram-dm1.png)

앱 오른쪽 상단에 종이 비행기 버튼으로 DM을 보낼 수 있습니다. 저는 계정을 바로 만들었기 때문에 DM을 보낸 이력이 없습니다. 그래서 메시지 시작으로 테스트를 한 번해보도록 하겠습니다.

![instagram-dm2](/assets/img/blog/instagram-dm2.png)

받는 사람에 `a`라고 타이핑을 했더니, 첫 번째에 **아리아나 그란데**가 나왔습니다. 그란데를 선택하고 다음을 눌렀습니다.

![instagram-dm3](/assets/img/blog/instagram-dm3.png)

메시지에 `Hi`라고 보내고, 정상적으로 보내진 것을 확인할 수 있습니다.

### 맺음

인스그램 PC 버전(instagram for PC) 사용하는 방법에 대해서 간단하게 알아보았습니다. 혹시나 궁금하신 점이나 이상한 점 있으면 댓글 부탁드리겠습니다.

감사합니다.
