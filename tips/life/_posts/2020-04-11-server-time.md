---
layout: base
title: 서버 시간 확인 (Check server time)
image: /assets/img/blog/server-time.png
description: >
  서버 시간 확인하기.
author: navy apple
comments: true
toc: true
permalink: /tips/life/server-time
tags:
  - server time
  - 서버시간
  - 네이비즘
---

## 서버 시간 확인 (Check server time)

티켓팅, 수강신청 등등 여러 서버들의 시간을 알아야하는 상황들이 있습니다. 
저도 수강신청할 때, 많이 사용했던었던 경험이 있네요. 그 외에도 게임, 티켓팅 등 많은 곳에서 
유용하게 사용할 수 있을 것 같습니다.

서버 시간을 확인할 수 있는 사이트들 중 3개의 사이트를 소개해드리려고 합니다.

<hr/>

## 서버시간 확인 사이트 TOP3

### 네이비즘 -> [바로가기](https://time.navyism.com)

![image](/assets/img/blog/st-navism.png)

저도 가장 많이 이용했던 사이트인 네이비즘 입니다. 수강신청 당일날 아침 9시부터 켜놓고, 기다렸던 기억이 있네요.

### 타임시커 -> [바로가기](https://timecker.com)

![image](/assets/img/blog/st-timecker.png)

아무래도 네이비즘 다음으로 많이 쓰는 사이트 일것이라고 생각됩니다.

### 서버타임Beta -> [바로가기](https://servertime.ze.am)

![image](/assets/img/blog/st-beta.png)

위 사이트들 모두 UI는 비슷한 것 같습니다.
해당 칸에 서버 주소를 쓰고 버튼을 누르면 해당 서버의 시간이 나오는 방식으로 구현이 되어있습니다.

예를 들어 인터파크에서 판매하는 티켓을 구매하려고 한다면, 인터파크 티켓 서버의 주소를 다음과 같이 넣고

![image](/assets/img/blog/st-navism2.png)

Fight 버튼을 누르면 해당 서버의 시간이 실시간으로 출력되도록 되어 있습니다.

그 외에도 yes24 티켓팅, 멜론 티켓팅, 티켓 링크 등등 다양한 서버의 시간대를 알 수 있습니다.

감사합니다.
