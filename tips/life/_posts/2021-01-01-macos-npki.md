---
layout: base
title: 맥(mac) 공인인증서 위치
image: /assets/img/blog/mac-finder.png
description: >
  맥(mac) 공인인증서 위치 및 브라우저에 공인인증서 가져오기
author: navy apple
comments: true
toc: true
permalink: /tips/life/macos-npki
tags:
  - macOS
  - 공인인증서
  - 인증서
  - 인증서 위치
---
	
## 맥(mac) 공인인증서 위치

몇년 전만해도 은행권에서 공인인증서를 발급 받거나 재발급을 받을 때 반드시 Windows OS, Internet explorer로 
진행해야만 하는 문제들이 있었습니다. 그런데 요즘들어 한국 macOS 사용자가 늘고 있고, 사용자들이 공인인증서 발급에 대한
불만이 늘어나게 된 것 같습니다. 그래서 공인인증서를 발행하는 기관에서 macOS에 대한 지원도 해주고 있는 추세입니다. 

macOS에서 공인 인증서를 하드디스크에 저장하고 쓰는 경우, 자동으로 저장되는 경로가 있는데요. 이 경로에 대해서 
알아보려고 합니다.

### 브라우저 공인인증서로 인증서 가져오기

![browser](/assets/img/blog/npkicontent1.png)

요즘에는 브라우저의 보안이 강해져서 브라우저에 공인인증서를 등록하고 사용할 수 있습니다. 위 그림에서 `브라우저로 공인인증서 가져오기`
버튼을 누르고 공인 인증서를 등록할 수 있습니다.

하지만 대부분 공인인증서가 어디있는지 모르실겁니다.

### 공인인증서 위치

#### 경로 이동 하기

{% include base/components/link-box-custom-url.html title='맥에서 경로 입력해서 폴더로 이동하기(mac finder path to folder)' description='맥에서 경로 입력해서 폴더로 이동하기(mac finder path to folder)' url='navy-apple.com' internal_url='/tips/life/mac-finder-path' bg_img_url='/assets/img/blog/mac-finder.png' %}

경로는 다음을 복사해서 이동해주세요.

```
~/library/Preferences/NPKI
```

![cert_path](/assets/img/blog/npkicontent2.png)

경로로 이동해서 `yessign`라는 디렉토리를 확인하실 수 있습니다.<br>
yessign 디렉토리에서 계속 들어가다보면 아래와 같은 디렉토리를 확인하실 수 있습니다.

#### 인증서 선택하기

![cert](/assets/img/blog/npkicontent3.png)

`signCert.der`과 `signPri.key`이 있는 것을 확인하실 수 있는데요. 이 것들이 인증서의 실체입니다.
이 인증서를 조금 전에 `가져오기` 버튼을 눌러 선택해줍니다.

이렇게 맥에서 공인인증서 위치를 확인할 수 있었습니다.

감사합니다.
