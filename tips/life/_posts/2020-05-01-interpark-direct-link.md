---
layout: base
title: "[Interpark Direct Link]인터파크 티켓팅 직링"
image: /assets/img/blog/interpark.png
description: >
  [Interpark Direct Link]인터파크 티켓팅 직링
author: navy apple
comments: true
toc: true
permalink: /tips/life/interpark-direct-link
tags:
  - 직링
  - 인터파크
  - 티켓팅
---

## [Interpark Direct Link]인터파크 티켓팅 직링

인터파크에서 티켓팅을 하는 경우가 많은데요. `클릭 클릭 클릭`해서 들어가는 것보다
`직링(Direct Link)`을 이용하면 가장 빠르게 접근이 가능한데요.

인터파크에서 직링을 얻는 방법은 따로 작성해 놓은 곳이 있습니다. 좀더 자세하게 정리해서
링크 걸어둘 예정이고, 업데이트 되는데로 바로 올리겠습니다.

그럼 직링에 대해서 정리해보겠습니다.

### SM 직링(Direct Link)

```
https://smticket.interpark.com/order/ticket/play_date.html?GOODS_CODE=<고유번호>
```

## 마무리

인터파크 직링에 대해서 알아보았습니다.

감사합니다.

{% include base/components/link-box-custom-url.html title='인터파크 직링 추출 하기' description='인터파크에서 직링 추출하는 방법에 대해서 소개해드립니다.' url='navy-apple.com' internal_url='/tips/life/interpark-direct-link' bg_img_url='/assets/img/blog/interpark.png' %}
