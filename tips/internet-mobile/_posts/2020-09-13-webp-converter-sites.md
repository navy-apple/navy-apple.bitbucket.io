---
layout: base
title: 이미지를 Webp로 변환 해주는 사이트 TOP3
image: /assets/img/blog/webp-converter-sites.png
description: >
  png, jpg, gif 등을 Webp로 변환해주는 사이트들의 장단점을 비교해보고 순위를 정해보도록 하겠습니다.
author: navy apple
comments: true
toc: true
permalink: /tips/internet-mobile/webp-converter-sites
tags: 
  - convert webp
  - image to webp
  - 이미지를 Webp
  - png to webp
  - jpg to webp
  - gif to webp
---

## 이미지를 Webp로 변환 해주는 사이트 TOP3

요즘에는 사진의 화소가 올라가면서 HD, Full HD, Ultra HD 등 사진의 사이즈가 날이 갈수록 커지고 있습니다. 사진이 커지면 사진을 보는 사람들은 좋지만, 사진을 보여주는 웹이나 앱에서는 큰 용량의 이미지를 렌더링하기 위해서 더 많은 네트워크 I/O를 수행해야합니다.

용량이 큰 이미지를 렌더링하다보면 다음과 같은 문제가 발생할 수 있습니다.

- 이미지가 로딩되는 동안 잘려진 이미지가 보일 수 있습니다.
- 이미지를 로딩 때문에 웹 페이지의 전체 속도가 느려질 수 있습니다.

<hr>

우선 이미지가 로딩되는 동안 잘려진 이미지가 보이는 것을 방지하기 위해서 `lazy loading` 방식을 사용할 수 있습니다. 이는 이미지의 렌더링 준비가 될 때까지 상대적으로 용량이 적은 미리 로딩된 이미지를 띄워 사용자에게 이미지가 로딩 중인 것을 알려주는 방식입니다.

예를 들어 다음과 같은 이미지 입니다.

| ![amp-lazy-loading](/assets/img/blog/amp-lazy-loading.jpg) | ![customized-lazy-loading](/assets/img/blog/customized-lazy-loading.png) |
|:-:|:-:|
| AMP Img lazy loading | Customized lazy loading |

하지만 lazy loading 방식은 웹 페이지의 속도가 느려지는 현상은 해결하는 방법은 아닙니다. 속도 문제를 해결하기 위해서는 이미지 용량 자체를 줄여야합니다. 그러기 위해서 생각해볼 수 있는 방법은 크게 두 가지가 있습니다.

- 이미지 자체의 너비와 높이를 줄이는 방법

{% include base/components/link-box-custom-url.html title='이미지 크기 조절을 온라인에서 한번에 | Image Resizer' description='png, jpeg, jpg, gif, bmp 등의 다양한 타입의 여러 이미지의 사이즈를 온라인에서 한 번에 조절하기' url='imgresizing.com' internal_url='https://imgresizing.com' bg_img_url='/assets/img/blog/image-resizer-link.png' %}

- 이미지를 압축하는 방법

{% include base/components/link-box-custom-url.html title='여러 이미지 압축 온라인에서 한 번에 | Image Compressor' description='png, jpeg, jpg, gif, bmp 등의 다양한 타입의 여러 이미지를 온라인에서 한번에 압축하기' url='navy-apple.com' internal_url='https://imgcompression.com' bg_img_url='/assets/img/blog/image-compressor-link.png' %}

보통은 위와 같은 방식으로 이미지 너비와 높이를 줄이거나, 이미지를 압축하는 방법을 사용하게 됩니다. 하지만 너비와 높이를 조절하지 못하는 경우나, 이미지의 화질을 생각하면 위 두 가지 방법이 마음에 들지 않을 수도 있습니다.

가장 현명한 방법은 이미지의 크기는 그대로 유지하고, 화질을 떨어뜨리지 않는 압축 알고리즘을 사용하는 것입니다. 하지만 그런 압축 알고리즘의 경우, 대부분 용량을 크게 줄여주지는 못합니다.

<hr>

그래서 이번 글에서 소개해드릴 것은 화질은 그대로 유지하면서 압축의 성능을 극대화시킨 이미지 타입인 `.webp` 라는 이미지 파일 형식 및 일반 이미지(jpg, png, gif, bmp)들을 webp로 변환해주는 사이트에 대한 것입니다.

webp 이미지 파일은 구글에서 개발한 웹 이미지 형식입니다. 이름에서도 web picture에서 따온 것으로 알고 있습니다. 이 파일 형식을 이용하면 이미지의 화질을 그대로 유지시켜주면서 파일의 용량은 대폭 줄일 수 있어서, 웹 페이지 로딩속도에도 큰 도움을 줄 수 있습니다.

하지만 `.webp` 이미지 형식의 단점은 구형 브라우저에서는 인식을 하지 못한다는 단점이 있습니다. 많은 사이트들에서 이를 해결하기 위해서 일반 이미지 파일과 webp 파일 동일한 두 이미지를 가지고 있고, webp 를 지원하지 않는 브라우저에는 일반 이미지를 노출하는 방식을 사용하고 있습니다.

<hr>

일반 이미지를 webp로 변경하기 위해서 구글에서 코드와 툴을 제공하고 있지만, 사용하기 편리한 사이트를 추려서 소개드려보겠습니다.

### [Image To Webp](https://imagetowebp.com)

{% include base/components/link-box-custom-url.html title='여러 이미지를 webp 변환 온라인에서 한 번에 | Image To Webp' description='png, jpeg, jpg, gif, bmp 등의 다양한 타입의 여러 이미지를 온라인에서 한번에 webp로 변환하기' url='imagetowebp.com' internal_url='https://imagetowebp.com' bg_img_url='/assets/img/blog/image-to-webp-link.png' %}

> - 여러 이미지 한번에 변환: 제한 없음
> - 사용하기 편리한 UX/UI: 좋음
> - 이미지 변환 속도: 매우 빠름
> - 국가별 언어지원 여부: 지원

### Tiny Img

{% include base/components/link-box-custom-url.html title='TinyIMG WebP Converter | TinyIMG' description='Use TinyIMG to convert your Shopify images to WebP format and instantly make your file sizes around 30% smaller!' url='tiny-img.com' internal_url='https://tiny-img.com/webp/' bg_img_url='/assets/img/blog/tiny-img-link.png' no_open_fw_ref=true %}

> - 여러 이미지 한번에 변환: 최대 10개
> - 사용하기 편리한 UX/UI: 좋음
> - 이미지 변환 속도: 느림
> - 국가별 언어지원 여부: 미지원

### Webp Converter

{% include base/components/link-box-custom-url.html title='WebP Converter' description='Convert your image pictures into the more efficient WebP format. WebP is supported by Chrome, Firefox, Edge and Safari from version 14.' url='webp-converter.com' internal_url='https://webp-converter.com' bg_img_url='/assets/img/no-image.png' no_open_fw_ref=true %}

> - 여러 이미지 한번에 변환: 불가능
> - 사용하기 편리한 UX/UI: 좋음
> - 이미지 변환 속도: 빠름
> - 국가별 언어지원 여부: 지원

### 맺음

이미지의 용량을 줄이는 방법 및 새로운 압축 알고리즘이 적용된 webp에 대해서 알아보았습니다. 뿐만 아니라 다양한 이미지를 webp로 변환시켜주는 3가지의 사이트를 소개해드렸습니다. 혹시 궁금하신 점이나 이상한 점이 있으시면 댓글 부탁드리겠습니다.
