---
layout: base
title: 편의점 유심 개통 (CU hello mobile)
image: /assets/img/blog/hellovision.png
description: >
  편의점 유심 개통 하는 방법 (씨유 헬로모바일)
author: navy apple
comments: true
toc: true
permalink: /tips/internet-mobile/convenience-store-usim
tags:
  - USIM
  - 유심
  - 편의점
  - 헬로모바일
  - hello mobile
---

## 편의점 유심 개통 (CU hello mobile)

예전에는 폰을 구입할 때, 약정을 걸고 샀었습니다. 그래서 2년동안 묶여서 이도저도 못한고 있었습니다.
그런데, 아주 재밌고 유용한 `USIM`이 나왔습니다. 알뜰폰 사업자인 **헬로모바일**과 **CU편의점**이 제휴를 맺어서
나온 `USIM`이 있습니다. 

오늘은 `헬로모바일 CU 유심`에 대해서 알아보겠습니다.

### USIM 구입하기

저는 좀 성격이 급한편이라 배송받고 하는 시간을 못 견디겠더라고요, 그런데 편의점에서 유심을 판다니! 그야말로
**신세계**였습니다. (물론 유심을 배송받는 방법으로도 개통을 할 수 있습니다.)

그래서 바로 달려가서 샀습니다.

| ![image](/assets/img/blog/buy-usim.png) | ![image](/assets/img/blog/uplus3.png) |
|:-:|:-:|
| 헬로모바일 CU 유심 | 내부 사용 설명 |

구입을 하고 열어보시면, 자세하게 설명이 되어 있습니다. 다음과 같이 하시면 됩니다.

- 우선, [헬로모바일 다이렉트](http://direct.lghellovision.net)로 이동합니다.

| ![image](/assets/img/blog/uplus4.png) | ![image](/assets/img/blog/buy-usim2.png) |
|:-:|:-:|
| [CU 요금제] -> [가입신청] 클릭 | 요금제 선택 화면 |

- `가입 신청`화면으로 진입하신 후, 요금제 선택 화면에서 원하는 요금제를 선택합니다.

| ![image](/assets/img/blog/uplus-usim-page1.png) | ![image](/assets/img/blog/usim-serial.png) |
|:-:|:-:|
| 구매방식, 가입유형, 일련번호 입력 | 유심 뒷편의 일련번호 |

- 구매방식, 가입유형을 각각 선택하고, 유심 뒷편의 일련번호를 형광색으로 표시한 곳에 입력 후, 온라인으로 즉시 가입 버튼을 누릅니다.

### 신청완료

이후에는 개인정보들을 입력하는 것들이라서 생략하겠습니다.

개인 정보는 크게 두 가지만 있으면 신청할 수 있습니다.

- **신용카드 정보**
- **신분증 정보**

만약 신용카드가 없으시다면, 체크카드로도 가능하지만 전화로 개통신청을 해야합니다.

이후에 개통이 완료 되었으면, 자신의 폰에 유심을 삽입하고 전원을 `3~4`회 정도 껏다 켯다를 반복하면 됩니다.

### 마무리

편의점에서 유심을 구입하여 개통하는 방법에 대해서 알아보았습니다.<br>
감사합니다.








