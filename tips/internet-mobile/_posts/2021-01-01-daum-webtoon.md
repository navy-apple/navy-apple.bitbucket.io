---
layout: base
title: 다음 웹툰 추천 BEST 7
image: /assets/img/blog/daum.png
description: >
  다음에서 연재되는 다양한 웹툰들을 소개하고 제 개인 취향에 맞춰서 순위를 결정했습니다.
author: navy apple
comments: true
toc: true
permalink: /tips/internet-mobile/daum-webtoon
tags: 
  - 다음
  - 웹툰
  - 다음 웹툰 추천
  - 이태원 클라쓰
---

## 다음 웹툰 추천 BEST 7

**주의!** 이 추천은 지극히 개인적이고 주관적인 추천 랭킹입니다. 뭐라고 하셔도 할말은 없으니 그냥 재미로 봐주셨으면 좋겠습니다.

저도 처음에는 네이버에 있는 웹툰들을 많이 봤었는데, 뭔가 네이버는 저랑은 성격이 안맞았던 것 같았습니다. 지금 네이버에서 보고 있는 웹툰은 `신의 탑`, `고수`, `쿠베라` 이렇게 세 개 정도가 있네요.

사실 상, 다음 웹툰에서 더 많이 보고 있었습니다. 자 그러면 바로 추천해드리도록 하겠습니다.

{% assign domain = 'webtoon.daum.net' %}
{% assign daum_link = '/assets/img/blog/daum-link.png' %}

### 이태원 클라쓰

![itaewon class](/assets/img/blog/itaewon.png)

- 작가: 광진
- 장르: 드라마, 청춘, 술

{% include base/components/link-box-custom-url.html title='이태원 클라쓰 | Daum 웹툰' description='각자의 가치관이 어우러지는 이 곳, 이태원. 이 거리를 살아가는 그들의 이야기' url=domain internal_url='http://webtoon.daum.net/webtoon/view/ItaewonClass' bg_img_url=daum_link no_open_fw_ref=true %}

2020년 현재, 드라마화가 되어서 JTBC에서 방영 중이라고 하는데요, 저는 [무료웹툰](https://eli.kr/rank/free-webtoon){:target="_blank"}으로 전편 다 봤었던 웹툰입니다.
내용은 전반적으로 캐주얼한 느낌이 있습니다. 그림채도 나쁘지 않아서 끌렸었던 것 같아요.

저는 원래 이런류의 웹툰을 안봤었는데 은근히 몰입력이 있는 웹툰이었습니다. 내용을 다 말씀 드릴 수는 없지만
통쾌한 복수극을 좋아하시는 분이라면 한 번쯤 봐보시라고 추천 드립니다.

### 후크

![hook](/assets/img/blog/hook.png)

- 작가: 나진수
- 장르: 판타지, 액션, 혁명

{% include base/components/link-box-custom-url.html title='후크 | Daum 웹툰' description='마법사와 마도 공학자들의 끝없는 전쟁. 몰락한 네버랜딩을 구하기 위한 혁명이 시작된다.' url=domain internal_url='http://webtoon.daum.net/webtoon/view/TheHook' bg_img_url=daum_link no_open_fw_ref=true %}

가끔 댓글 같은 곳에 보면, **다음의 신의 탑이다** 라는 평가를 받고 있을 정도로 다음 웹툰에서 인기가 많은 웹툰입니다.
저도 결해서 미리보기로 꼭꼭 챙겨보는 웹툰입니다.

마도 공학, 마법사, 검사 등 판타지 성격이 강한 스토리로 진행됩니다. 그리고 약간의 개그 요소가 있어서
너무 진지한 느낌의 웹툰은 또 아닙니다. 그리고 또 작가님이 스토리에 대한 생각을 많이하셔서 가끔 깜짝깜짝 놀랄만한 반전 요소들을 곳곳에 배치해 두어서 반전을 보는 재미도 쏠쏠합니다.

판타지나 액션, 전략 등을 좋아하시는 분이라면 꼭 보시라고 추천드립니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.first.infeed %}

### 레드스톰

![redstorm](/assets/img/blog/redstorm.png)

- 작가: 암현
- 장르: 액션, 무협, 무장

{% include base/components/link-box-custom-url.html title='레드스톰 | Daum 웹툰' description='사막의 찐득찐득한 모래바람과, 그곳을 질주하는 전사들의 이야기' url=domain internal_url='http://webtoon.daum.net/webtoon/view/redstorm' bg_img_url=daum_link no_open_fw_ref=true %}

연재된지 그래도 몇년은 된 작품입니다. 하지만 내용, 스토리 구성 등이 휼륭한 웹툰이었습니다. 예전에 토요웹툰하면
**레드스톰**과 **무장** 투톱으로 불렸었습니다. 보시면 흑백 웹툰인데도 인기가  엄청 많았고, 저도 지금도 가끔 생각나면 보고 있습니다.

배경은 사막이고 전투, 전술 전략, 정치, 무신, 포스마스터 이런 종류의 내용이 주를 이루고 있습니다. 내용도 탄탄하고, 지루함이 없는 작품으로 **무협**장르를 좋아하신다면 꼭 추천드립니다.

### 블랙 베히모스

![black](/assets/img/blog/black.png)

- 작가: 케이지콘
- 장르: 판타지, 액션, 전투

{% include base/components/link-box-custom-url.html title='블랙 베히모스 | Daum 웹툰' description='탈리스만이 되기 위한 시험에 도전하는 주인공의 힘겨운 전투' url=domain internal_url='http://webtoon.daum.net/webtoon/view/blackbehemoth' bg_img_url=daum_link no_open_fw_ref=true %}

블랙 베히모스, 이건 제가 정주행 2번 정도한 웹툰입니다. 왜 2번 했냐면 워나 길어서 기억이 안나는 부분도 있고, 과거의 떡밥을 확인하기 위한 부분도 있었습니다. 이 작품은 초반부부터 떡밥을 곳곳에 뿌려 놓고 나중에 회수를 하면서 "와 ~ 이거 였어?"하게 만드는 웹툰입니다.

내용도 탄탄할 뿐아니라 과학적인 내용도 포함이 되어있는데요. 특히 **천체 물리학**을 지루하지 않게 적절히 녹여내면서 놀라움을 만들어 내는 작품입니다. 예를 들어 **차원**에 관한 내용들이나 중력, 별 등을 판타지로 녹여내었습니다.

판타지, 전략, 액션을 좋아하신다면 꼭 보시라고 추천드립니다.

### 아비무쌍

![abimussang](/assets/img/blog/abi.png)

- 작가: 노경찬(글), 이현석(그림)
- 장르: 무협, 판타지, 무장

{% include base/components/link-box-custom-url.html title='아비무쌍 | Daum 웹툰' description='하늘의 뜻을 아니 아비는 무쌍이다.' url=domain internal_url='http://webtoon.daum.net/webtoon/view/Matchless' bg_img_url=daum_link no_open_fw_ref=true %}

무협 장르의 경우는 보통 소설로 많이 나오는데요. 아마도 글, 그림을 따로 하신 것으로 보아 원래는 소설이 었다가 웹툰 화된 것 같습니다. 네이버에 웹툰 **고수**가 있다면 다음에는 **아비무쌍**이 있습니다. 무협장르에서 많이 나오는 형식인 실제로는 고수인데 밖으로 나와서는 고수라는 것이 알려지지 않은 상태로 이야기가 진행됩니다.

저도 이런 주인공이 겸손(?)한 내용의 무협을 좋아합니다. **무협**을 좋아하시는 분이라면 추천드리겠습니다.

### SAVAGE

![savege](/assets/img/blog/savege.png)

- 작가: 홍경표
- 장르: 스릴러, 액션, 범죄

{% include base/components/link-box-custom-url.html title='SAVAGE | Daum 웹툰' description='야만의 시대, 욕망을 향한 짐승의 몸부림이 시작된다.' url=domain internal_url='http://webtoon.daum.net/webtoon/view/savage' bg_img_url=daum_link no_open_fw_ref=true %}

이 웹툰은 최근에 연재가 시작된 웹툰인데요, 가끔 스릴러나 범죄물에도 흥미가 이었던적이 있습니다. 그 중에 1화를 보자마자 "오, 이거 재밌겠는데?"하는 생각을 하며 보기시작한 웹툰입니다. 그림체는 약간 이태원 클라쓰와 비슷한 느낌이고, 영화같은 느낌을 주는 웹툰입니다.

스릴러라는 장르답게 사람을 궁금하게 만드는 스토리 전개, 주인공의 심리 등을 잘 묘사해놓았습니다.
실제로 현실에서 이런 내용이 일어나고 있을 것만 같은 느낌?이 드는 웹툰입니다.

스릴러, 범죄 등의 장르를 좋아하신다면 추천드립니다.

### 풍검

![poonggum](/assets/img/blog/poonggum.png)

- 작가: 김철현
- 장르: 액션, 판타지, 동양

{% include base/components/link-box-custom-url.html title='풍검 | Daum 웹툰' description='마계의 힘으로 인간계를 없애려는 자들과 막으려는 자들의 치열한 싸움' url=domain internal_url='http://webtoon.daum.net/webtoon/view/windsword' bg_img_url=daum_link no_open_fw_ref=true %}

무장 작가님이신 **김철현**작가님의 작품으로 무장에서는 완전 **무협**이 었다면, 이번 풍검에서는 판다지적인 요소를 많이 첨가한 작품입니다. 색이 없는 흑백 웹툰이지만, 내용과 그림체 모두 훌륭한 작품입니다.

저도 최근까지 미리보기로 결제해가며 봤었는데, 일이 바빠져서 결제까지는 못하고 무료 회차를 보고 있습니다.
판타지 요소가 가미되서 내용도 풍부해지고 조금 더 캐주얼 해진 느낌의 웹툰입니다.

또 과거에 **무장**이라는 웹툰을 좋아하셨다면 추천드립니다.

#### 맺음

이렇게 **다음 웹툰 추천 TOP7**을 정리해보았는데요. 지극히 제 개인적인 취향, 주관에 의해서 결정했습니다.
저와 비슷한 취향을 가지고 계신분 들에게 도움이 되길 바랍니다.

감사합니다.

```diff
# 이미지 출처: webtoon.daum.net
```
