---
layout: base
title: "[Solved] Favicon is not showing"
image: /assets/img/blog/mobile.png
description: >
  Favicon is not showing in mobile google search result
author: navy apple
comments: true
toc: true
permalink: /tips/internet-mobile/favicon-is-not-showing
tags:
  - favicon
  - showing
  - google
  - search
  - chrome
  - mobile google search
---

## [Solved] Favicon is not showing

저의 Jekyll로 만들어진 블로그 또한 `Favicon`이 보이지 않는 현상이 있었습니다.
아래 그림처럼 말이죠.

|![image](/assets/img/blog/favicon-result1.png)|
|:-:|
| Favicon is not showing |

이번에는 왜 모바일 크롬에서 favicon이 보이지 않는지? 그리고 어떻게 하면 보이게 할 수 있는지 알아보도록 하겠습니다.

### Reason

Favicon이 보이지 않는 이유는 구글 크롤링이 인식을 하지 못해서 인데요, 저는 다음과 같은 방법으로 해결 했습니다.


- AS-IS

```html
<link rel="icon" type="image/x-icon" href="/assets/icons/favicon.ico">
```

- TO-BE

`favicon.ico`를 루트 디렉토리에 복사하고, 다음과 같이 코드를 변경해줍니다.

```html
<link rel="icon" type="image/x-icon" href="/favicon.ico">
```

그리고 구글 크롤러가 크롤링을 할 수 있는 여유시간이 필요합니다. 저같은 경우 `6 ~ 12` 시간 정도 걸린 것 같습니다.

### Result

| ![image](/assets/img/blog/favicon-result1.png) | ![image](/assets/img/blog/favicon-result2.png)|
|:-:|:-:|
| Before | After |

감사합니다.
