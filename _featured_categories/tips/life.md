---
layout: base-list
title: 라이프 (Life)
slug: life
menu: true
description: >
  일상 생활에서 알 수 있는 여러가지 팁들을 정리하여 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 생활
  - 생활 팁
  - 영화
  - 취미
---
