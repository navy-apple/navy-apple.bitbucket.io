---
layout: base-list
title: 인터넷/모바일 (internet/mobile)
slug: internet-mobile
menu: true
description: >
  인터넷 또는 모바일에서 알 수 있는 여러가지 팁들을 정리하여 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 인터넷
  - 모바일
  - 웹툰
  - 다시보기
---
