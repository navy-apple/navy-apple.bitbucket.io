---
layout: base-list
title: 코틀린 (Kotlin)
slug: dev-kotlin
menu: true
description: >
  여러 예제를 통해 Kotlin에 대해서 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 개발
  - 코틀린
  - kotlin
  - kotlin developing
---
