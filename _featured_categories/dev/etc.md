---
layout: base-list
title: 기타 (Etc)
slug: dev-etc
menu: true
description: >
  분류되지 않은 개발 관련된 것들에 대해서 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 개발
  - develop
  - developing
  - js
---
