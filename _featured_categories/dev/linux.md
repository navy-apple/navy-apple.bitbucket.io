---
layout: base-list
title: 리눅스 (Linux)
slug: linux
menu: true
description: >
  예제를 통하여 Linux에서 사용하는 여러가지 것들에 대해서 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 개발
  - 리눅스
  - linux
  - linux programming
---
