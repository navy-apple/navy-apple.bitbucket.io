---
layout: base-list
title: software expert
slug: software-expert
menu: true
description: >
  software expert의 문제 풀이
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 알고리즘
  - 소프트웨어 익스퍼트
  - 삼성 알고리즘
  - 삼성
---
