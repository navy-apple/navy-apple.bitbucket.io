---
layout: base-list
title: 기타 에러 (Etc Errors)
slug: etc
menu: true
description: >
  분류 되지 않은 다양한 에러들에 대해 소개해드립니다.
accent_color: rgb(38,139,210)
accent_image: /assets/img/sidebar-bg.jpg
overlay: false
tags:
  - 에러
  - 트러블 슈팅
  - error
  - 미분류 에러
---
