window.addEventListener('DOMContentLoaded', function () {
  checkWebpFeature(function (isAvailableWebp) {
    initReactiveImageLoad(isAvailableWebp);
  });
});

var initReactiveImageLoad = function (isAvailableWebp) {
  var observer = new IntersectionObserver(function (entries) {
    entries.forEach(entry => {
      if (entry.intersectionRatio > 0) {
        if (entry.isIntersecting) {
          observer.unobserve(entry.target);
          loadImage(replaceImage, entry.target, entry.target.getAttribute(isAvailableWebp ? 'src' : 'alternative-src'));
        }
      }
    });
  });

  var reactiveImages = document.querySelectorAll('reactive-img');
  reactiveImages.forEach(function (entry) { observer.observe(entry); });
}

var checkWebpFeature = function (callback) {
  var img = new Image();
  img.onload = function () { callback((img.width > 0) && (img.height > 0)); };
  img.onerror = function () { callback(false); };
  img.src = "data:image/webp;base64,UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA";
}

var loadImage = function (callback, target, src) {
  var img = new Image();
  img.onload = function () { callback(target, img); };
  img.onerror = function () { callback(false); };
  img.src = src;
}

var replaceImage = function (reactiveImage, image) {
  var loading = reactiveImage.lastElementChild;
  if (reactiveImage) {
    reactiveImage.style.paddingTop = 0;
    reactiveImage.removeChild(loading);
    reactiveImage.appendChild(image);
  }
}
