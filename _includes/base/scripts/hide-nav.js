var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (currentScrollPos < 30) {
    document.getElementById("_navbar").style.top = "0";
    return;
  }

  if (prevScrollpos > currentScrollPos) {
    document.getElementById("_navbar").style.top = "0";
  } else {
    document.getElementById("_navbar").style.top = "-60px";
  }
  prevScrollpos = currentScrollPos;
}
