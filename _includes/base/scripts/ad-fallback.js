var createAd = function () {
  var adATag = document.createElement('a');
  adATag.href = 'https://kmong.com/gig/319020';
  adATag.target = '_blank';

  var adImg = document.createElement('img');
  adImg.src = '/assets/img/blog/jett-ad.png';
  adImg.style.width = '100%';
  adATag.appendChild(adImg);
  return adATag;
}

var addCustomAd = function () {
  var adAreas = document.querySelectorAll('ins.adsbygoogle[data-ad-status="unfilled"]:not(.adsbygoogle-noablate)');
  for (var i = 0; i < adAreas.length; i += 1) {
    var parent = adAreas[i].parentElement;
    var next = adAreas[i].nextElementSibling;
    parent.insertBefore(createAd(), next);
  }
}

setTimeout(addCustomAd, 2500);