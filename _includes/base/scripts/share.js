var isLoadedKakaoSDK = false;
document.querySelector('.at-expanding-share-button-toggle-bg').addEventListener('click', function (e) {
  var shareBox = document.querySelector('#at-expanding-share-button');
  if (shareBox.classList.contains('at-expanding-share-button-show-icons')) {
    shareBox.classList.remove('at-expanding-share-button-show-icons');
  } else {
    shareBox.classList.add('at-expanding-share-button-show-icons');
  }
  if (!isLoadedKakaoSDK) {
    loadKakaoSDK(false);
  }
});

var removeQueryString = function () {
  var newURL = location.href.split("?")[0];
  window.history.replaceState(null, null, newURL);
}

document.querySelector('.at-share-btn.at-svc-kakaotalk').addEventListener('click', function () {
  shareKakao();
});

var shareKakao = function () {
  if (isLoadedKakaoSDK) {
    Kakao.Link.sendCustom({
      templateId: 29744,
      templateArgs: {
        thumbnail: kakaoThumbnail,
        title: kakaoTitle,
        description: kakaoDescription,
        path: kakaoPath
      }
    });
  }
};

document.querySelector('.at-share-btn.at-svc-link').addEventListener('click', function () {
  var input = document.getElementById('copy-target-input');
  input.value = siteUrl + '/' + kakaoPath;
  input.hidden = false;
  input.select();
  document.execCommand('copy');
  input.hidden = true;
  var toast = document.querySelector('.toast-alert');
  toast.classList.remove('active');
  void toast.offsetWidth;
  toast.classList.add('active');
}, false);

var loadKakaoSDK = function (isRunImmediately) {
  var script = document.createElement('script');
    script.setAttribute('src', '/assets/js/kakao.min.js');
    script.setAttribute('type', 'text/javascript');
    script.addEventListener('load', function(e) {
      Kakao.init('1497f24429bc846610910b99414fd76c');
      isLoadedKakaoSDK = true;
      if (isRunImmediately) {
        shareKakao();
      }
    });
    document.head.appendChild(script);
};

(function () {
  var qObject = {};
  var queries = window.location.search.substr(1).split('&');
  for (i in queries) {
    q = queries[i].split('=');
    qObject[decodeURIComponent(q[0])] = decodeURIComponent(q[1]);
  }
  if (qObject.share && qObject.share === 'kakao') {
    loadKakaoSDK(true);
  }
  removeQueryString();
})();
