var navTitles = document.getElementsByClassName('nav-title');

for (var n = 0; n < navTitles.length; n += 1) {
  navTitles[n].addEventListener('click', function (e) {
    openSubtitles(e.target.getAttribute('nav-id'));
  });
}

function openSubtitles (id) {
    var index = getNavTitleIndex(id);
    if (index === -1) return;
    var isSameIndex = false;
    for (var i = 0; i < navTitles.length; i += 1) {
        if (navTitles[i].parentNode.classList.contains('active')) {
            if (i === index) isSameIndex = true;
            navTitles[i].parentNode.classList.remove('active');
        }
    }
    if (!isSameIndex) navTitles[index].parentNode.classList.add('active');
}

function getNavTitleIndex (id) {
    switch (id) {
        case 'tips': return 0;
        case 'dev': return 1;
        case 'errors': return 2;
        case 'algorithms': return 3;
        default: return -1;
    }
}

function openOneDepth () {
    var path = window.location.pathname;
    var parts = path.split('/');
    if (parts.length < 2) return;
    var oneDepth = parts[1];
    var index = getNavTitleIndex(oneDepth);
    if (navTitles[index]) {
        navTitles[index].parentElement.classList.add('active');
        activateSubTitle(index, parts[2])
    }
}

function activateSubTitle (index, twoDepth) {
    var subTitles = navTitles[index].nextElementSibling.children, id;
    for (var i = 0; i < subTitles.length; i += 1) {
        id = subTitles[i].getAttribute('sub-nav-id');
        if (id.indexOf(twoDepth) > -1) {
            subTitles[i].classList.add('active');
            return;
        }
    }
}

openOneDepth();
