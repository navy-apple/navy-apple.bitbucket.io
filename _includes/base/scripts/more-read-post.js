function moreReadPost(btn) {
  var cards = btn.parentElement.querySelectorAll('.n-card-container.hidden')
  var end = cards.length < 6 ? cards.length : 6;
  for (var i = 0; i < end; i += 1) {
    cards[i].classList.remove('hidden');
  }
  var isAllread = btn.parentElement.querySelectorAll('.n-card-container.hidden').length === 0;
  if (isAllread) {
    btn.parentElement.removeChild(btn);
  }
}