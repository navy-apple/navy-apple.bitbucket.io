var openButton = document.querySelector('.menu-open');
var closeButton = document.querySelector('.menu-close');

openButton.addEventListener('click', function () {
  document.querySelector('.wide-sidebar').className = 'wide-sidebar active';
})

closeButton.addEventListener('click', function () {
  document.querySelector('.wide-sidebar').className = 'wide-sidebar';
});
