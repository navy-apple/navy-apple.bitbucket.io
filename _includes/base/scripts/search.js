var isSearching = false
var searchButton = document.getElementById('_search');
var searchContainer = document.getElementById('search-container');
var searchInput = document.getElementById('search-input');
var searchedPosts = document.getElementById('searched-posts');
var navLogo = document.querySelector('.nav-logo');
var searchWinow = document.querySelector('.float-search');
var searchSvg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#161A31" d="M13 8h-8v-1h8v1zm0 2h-8v-1h8v1zm-3 2h-5v-1h5v1zm11.172 12l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z"></path></svg>'
var closeSvg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#161A31" d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"></path></svg>'

var search = function (e) {
  if (lightJekyllSearch.posts.length === 0) lightJekyllSearch.initialize();
  searchButton.innerHTML = isSearching ? searchSvg : closeSvg;
  var searchComponents = [searchContainer, searchedPosts, searchWinow];
  if (isSearching) {
    searchComponents.forEach(function (target) {
      navLogo.classList.add('active');
      updatorActiveClass(target, 'REMOVE');
    });
    searchInput.value = '';
    lightJekyllSearch.search('');
  } else {
    searchComponents.forEach(function (target) {
      navLogo.classList.remove('active');
      updatorActiveClass(target, 'ADD');
    });
    searchInput.focus();
  }

  isSearching = !isSearching;
  if (e) e.preventDefault();
};

var updatorActiveClass = function (target, operator)  {
  if (operator === 'ADD') {
    target.classList.add('active');
  } else {
    target.classList.remove('active');
  }

}

var isPassive = function () {
  var supportsPassiveOption = false;
  try {
    addEventListener(
      'test',
      null,
      Object.defineProperty({}, 'passive', {
        get: function() {
          supportsPassiveOption = true;
        }
      })
    );
  } catch (e) {}
  return supportsPassiveOption;
}

searchButton.addEventListener('mousedown', search);
searchButton.addEventListener('touchstart', search, isPassive() ? { capture: false, passive: false } : false);
searchInput.addEventListener('blur', function (e) {
  if (!isSearching) return;
  setTimeout(search, 200);
});

searchInput.addEventListener('input', function (e) {
  lightJekyllSearch.search(e.target.value);
});

var getSearchQeury=function(t){for(var r=window.location.search.substring(1).split("&"),n=0;n<r.length;n++){var i=r[n].split("=");if(i[0]==t)return i[1]}return!1};
var goSearch = function () {
  var needOpen = getSearchQeury('s');
  if (needOpen) {
    var newURL = location.href.split("?")[0];
    window.history.replaceState(null, null, newURL);
    search();
  }
}

goSearch();
