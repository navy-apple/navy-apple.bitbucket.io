---
layout: base
published: false
title: Error selecting key or setting result to parameter object
image: /assets/img/blog/mybatis.png
description: >
  Error selecting key or setting result to parameter object
author: navy apple
comments: true
toc: true
permalink: /errors/mybatis/select-key-error
tags:
  - mybatis
  - select key
  - generated key
---

## Error selecting key or setting result to parameter object (SelectKey error)

selectKey를 사용할 때, 발생할 수 있는 에러들에 대해서 정리해보겠습니다.

### Error

`Cause: org.apache.ibatis.executor.ExecutorException: Error selecting key or setting result to parameter object`

### Reason
1. selectKey의 `resultType`이 일치하지 않는 경우.

```sql
<insert id="insertTest">
        INSERT INTO test_table(
            column1,
            column2,
            column3,
            column4
        )
        VALUES (
            #{column1},
            #{column2},
            #{column3},
            #{column4}
        )
        # check result type !!
        <selectKey keyColumn="id" keyProperty="id" resultType="int" order="AFTER">
            SELECT LAST_INSERT_ID() AS id
            FROM test_table
        </selectKey>
</insert>
```

2. selectKey의 return 값이 빈 경우.

```sql
<insert id="insertTest">
        INSERT INTO test_table(
            column1,
            column2,
            column3,
            column4
        )
        VALUES (
            #{column1},
            #{column2},
            #{column3},
            #{column4}
        )
        # check return value is not empty !
        <selectKey keyColumn="id" keyProperty="id" resultType="int" order="AFTER">
            SELECT LAST_INSERT_ID() AS id
            FROM test_table
        </selectKey>
</insert>
```

3. table에 자동으로 생성되는 Key property가 없는 경우. (ex mysql A.I)

### Solution

1. `resultType`의 data type을 일치 시켜준다.
2. selectKey 내의 조건을 잘 확인해서 잘 리턴 되는지 확인 한다.
3. `useGeneratedKey="false"` 설정을 해준다.
