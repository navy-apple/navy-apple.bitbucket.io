---
layout: base
title: "Module build failed: TypeError: this.getResolve is not a function 에러"
image: /assets/img/blog/vue.png
description: >
  "Vue.js: scss-loader error"
author: navy apple
comments: true
toc: true
permalink: /errors/etc/vue-scss-error
tags:
  - vue
  - webpack
  - scss
  - sass
  - sass loader
---

## Module build failed: TypeError: this.getResolve is not a function 에러

Module build failed: TypeError: this.getResolve is not a function - Vue.js: scss 적용시 발생하는 에러

### Install in vue.js

```
> npm i -D node-sass sass-loader
```

### Error

```
Module build failed: TypeError: this.getResolve is not a function
at ...
```

### Reason
vue-cli(webpack v3.x ~ 4.4x)로 생성된 프로젝트와 sass-loader 8.0 버전이
호환이 되지 않기 때문에 발생하는 것이었습니다.

### Solution

```
npm uninstall sass-loader
npm i -D sass-loader@7.3.1
```

refs: [github issue: webpack-contrib/sass-loader]

[github issue: webpack-contrib/sass-loader]: https://github.com/webpack-contrib/sass-loader/issues/761
