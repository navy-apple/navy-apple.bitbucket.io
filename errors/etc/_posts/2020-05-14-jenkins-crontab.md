---
layout: base
published: false
title: "spread load evenly by using rather than"
image: /assets/img/blog/jenkins.png
description: >
  "spread load evenly by using rather than 경고"
author: navy apple
comments: true
toc: true
permalink: /errors/etc/jenkins-crontab
tags:
  - jenkins
  - crontab
  - 젠킨스 
  - 크론탭
---

## spread load evenly by using rather than

젠킨스를 이용하여 배치 잡(Batch job)을 수행하려고 할 때, `spread load evenly by using rather than` 경고에 대해서 
알아보겠습니다.

### Warn

`spread load evenly by using rather than`

### Reason

```
To allow periodically scheduled tasks to produce even load on the system,
the symbol H (for “hash”) should be used wherever possible. 
For example, using 0 0 * * * for a dozen daily jobs will cause a large spike at midnight.
In contrast, using H H * * * would still execute each job once a day,
but not all at the same time, better using limited resources.
```

📎 [jenkins - Spread load evenly by using ‘H * * * *’ rather than ‘5 * * * *’ - Stack Overflow](https://stackoverflow.com/questions/26383778/spread-load-evenly-by-using-h-rather-than-5)

> 해석을 해보자면,

주기적으로 스케줄링된 작업이 시스템에 균등한 부하를 발생시킬 수 있도록 하기 위해서는 기호 H(Hash)를 가능한 한 사용해야 합니다.
예를 들어, 1일 작업 스케쥴링인 0 0 * * * 크론 표현식(cron expression)으로 동작하는 Job이 12개 있다고 한다면, 12시 정각에 큰 부하가 발생할 것입니다.<br/>
이와는 대조적으로, H H * * *를 사용하면 12개의 Job을 서로 다른 시간에 적절하게 나누어 실행하게 할 수 있습니다.
이는 시간이라는 자원을 효율적으로 사용하는 것에 해당할 것입니다.

이러한 내용이네요.

 결론적으로, 사실 작성하신대로 사용해도 되지만, `H` 표현식을 사용하면 더 좋다라는 것을 알려주는 경고입니다.<br/>
`H`를 사용하면, 시간을 적절히 분배해서 작동하도록 해준다고 생각하시면 됩니다.

### Tip

Hash에 기간을 설정해줄 수 있는데요.

```
H(1-10) * * * *
```

위와 같은 형태로 작성하면, 매시 1분 ~ 10분 사이에서 Job을 실행하도록 설정할 수 있습니다.
