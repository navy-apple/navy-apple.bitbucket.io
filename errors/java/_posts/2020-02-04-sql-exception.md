---
layout: base
published: false
title: exception java.sql.sqlexception is never thrown in body of corresponding try statement
image: /assets/img/blog/java.png
description: >
  exception java.sql.sqlexception is never thrown in body of corresponding try statement
author: navy apple
comments: true
toc: true
permalink: /errors/java/sql-exception
tags:
  - java
  - sql
  - sqlexception
  - exception
---

## exception java.sql.sqlexception is never thrown in body of corresponding try statement

`exception java.sql.sqlexception is never thrown in body of corresponding try statement` 에러가 
발생하는 경우에 대해서 정리해 보도록하겠습니다.

### error

```
exception java.sql.sqlexception is never thrown in 
body of corresponding try statement
```

### reaseon

아래와 같이 코드를 작성하였을 경우, try block 안에서 SQLException이 발생할 수 있는 코드가 작성되어 있어야 합니다.
그렇지 않은 경우, 위와 같은 에러가 발생합니다.

```java
try {
  // service code
} catch (SQLException e) {
  log.error(e.getMessage());
}
```

### solution

제 경우, Mybatis를 사용하고 있는데, 다음과 같이 Mybatis에 관련된 Exception으로 바꿔줍니다.

```java
try {
  // mybatis code
} catch (PersistenceException e) {
  log.error(e.getMessage());
}
```
