---
layout: base
published: false
title: Failed to determine a suitable driver class 에러
image: /assets/img/blog/springboot.png
description: >
  Failed to determine a suitable driver class 에러
author: navy apple
comments: true
toc: true
permalink: /errors/spring/datasource-error
tags:
  - spring
  - spring-boot
  - datasource
  - application.yml
---

## Failed to determine a suitable driver class 에러

`Failed to determine a suitable driver class` 에러에 대해서 알아보도록 하겠습니다.

### Error

```
***************************
APPLICATION FAILED TO START
***************************

Description:

Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be 
configured.

Reason: Failed to determine a suitable driver class


Action:

Consider the following:
	If you want an embedded database (H2, HSQL or Derby), please put it on the classpath.
	If you have database settings to be loaded from a particular profile you may need to activate it (no profiles 
are currently active).
```

### Reason

원인은 `datasource` 설정이 되어있지 않기 때문입니다.


### Solution

1. `spring boot`의 `@Autoconfiguraion`의 `exclude` 속성에 `DataSourceAutoConfiguration`을 추가합니다.

```kotlin
@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class])
class TestApplication

fun main(args: Array<String>) {
    runApplication<TestApplication>(*args)
}
```


2. `application.yml` 또는 `application.properties`에 `datasource`를 설정해줍니다.

```yml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/db_name?serverTimezone=UTC&characterEncoding=UTF-8&useSSL=false
    username: root
    password: root
```

간단하게 `Failed to determine a suitable driver class`에러에 대해서 알아보았습니다.

감사합니다.
