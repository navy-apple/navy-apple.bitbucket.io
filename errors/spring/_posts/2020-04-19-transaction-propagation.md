---
layout: base
title: Transaction rolled back because it has been marked as rollback-only 에러
image: /assets/img/blog/springboot.png
description: >
  Transaction rolled back because it has been marked as rollback-only 에러
author: navy apple
comments: true
toc: true
permalink: /errors/spring/transaction-propagation
tags:
  - spring batch
  - meta table
  - batch job instance
---


## Transaction rolled back because it has been marked as rollback-only 에러

Transaction rolled back because it has been marked as rollback-only 에러가 어떤 이유로 발생하는지
여러가지 Case에 대해서 알아보겠습니다.

### Error

```java
org.springframework.transaction.UnexpectedRollbackException: 
Transaction rolled back because it has been marked as rollback-only
```

### Reason

1. Commit failed while step execution data was already updated. Reverting to old version.

이 경우는 트랜젝션이 중첩되어 열려있어 내부 Transaction이 `Rollback-only` 상태를 마킹하였고, 외부 Transaction이 이를
예상하지 못했을 때, 발생합니다.

### Solution

1. propagation 속성에 `REQUIRES_NEW` or `NESTED` 옵션을 사용합니다.

옵션의 자세한 설명은 다음 링크를 통해 확인하실 수 있습니다.

{% include base/components/link-box-custom-url.html title='spring transactional propagation options(옵션)' description='spring transactional propagation 옵션' url='navy-apple.com' internal_url='/dev/spring/transactional-propagation' bg_img_url='/assets/img/blog/springboot.png' %}

감사합니다.
