---
layout: base
title: "[Linux] Bash echo로 파일 끝에 붙여쓰기 (append echo output to the end of a text file)"
image: /assets/img/blog/bash.png
description: >
  [Linux] Bash echo로 파일 끝에 붙여쓰기 (append echo output to the end of a text file)
author: navy apple
comments: true
toc: true
permalink: /dev/linux/echo-append-file
tags:
  - bash
  - echo
  - append
  - end of file
---

## [Linux] Bash echo로 파일 끝에 붙여쓰기 (append echo output to the end of a text file)

Bash에서 `echo` 명령어를 사용하는 경우가 있습니다.

아래 두 가지 경우가 흔히들 많이 사용하는 경우 입니다.

1. 무엇인가를 출력하고 싶은 경우
2. 무엇인가를 출력하여 파일에 쓰고 싶은 경우
 

### 예제 (example)

```
> echo "A beautiful day !!"
> A beautiful day !!
```

또는 다음과 같이 파일 입력을 위해 사용합니다.

```
> echo "A beautiful day !!" > days.txt

> vi days.txt

in file

A beautiful day !!


```

그런데, 이 후에 다시 다음과 같이 명령어를 실행하면, 파일의 내용이 덮어 씌어지게 됩니다.

```
> echo "A awesome day !!" > days.txt
```

만약 파일 뒤에 이어서 써지게 하고 싶다면 다음과 같이 해야합니다.

> 파일 뒤에 붙여쓰기 (append to end of file)

```
> echo "A awesome day !!" >> days.txt
```

`>` to `>>` 바꿔주시기만 하면 됩니다.

간단하게 bash에서 echo로 파일 끝에 붙여쓰기에 대해서 알아보았습니다.<br/>
감사합니다.
