---
layout: base
published: false
title: "[Linux] Bash 파일 생성 (Create file in linux bash)"
image: /assets/img/blog/bash.png
description: >
  [Linux] Bash 파일 생성 (Create file in linux bash)
author: navy apple
comments: true
toc: true
permalink: /dev/linux/bash-create-file
tags:
  - bash
  - file
  - create
---

## [Linux] Bash 파일 생성 (Create file in linux bash)

Bash에서 파일을 생성할 수 있는 방법에는 여러 가지가 있습니다. echo, vi, touch, nano 등이 있는데요.
각각을 이용하여 파일을 생성하는 방법을 알려드리도록 하겠습니다.

### touch

기본적으로 많이 사용하는 파일 생성 명령어 입니다. 다음과 같이 사용할 수 있습니다.

```
> touch [filename].txt
```

touch로 생성하는 경우, 현재 디렉토리에 빈 파일에 만들어지게 됩니다.

### echo

echo 명령어도 파일을 만들때 많이 사용하는 명령어로 내용을 파일에 넣으면서 만들 수 있다는 장점이 있습니다.

```
> echo "A beautiful day !!" > [filename].txt
```

파일을 만들고 그 이후에 파일 끝에 붙여 쓰기를 하고 싶다면 다음 링크를 통해 확인하실 수 있습니다.

{% include base/components/link-box-custom-url.html title='[Linux] Bash echo로 파일 끝에 붙여쓰기 (append echo output to the end of a text file)' description='[Linux] Bash echo로 파일 끝에 붙여쓰기 (append echo output to the end of a text file)' url='navy-apple.com' internal_url='/dev/spring/transactional-propagation' bg_img_url='/assets/img/blog/bash.png' %}

에서 확인하실 수 있습니다.

### vi (or vim)

이 방법은 `vi editor`를 이용하는 방법인데요, 에디터를 이용해 파일을 작성해서 만드는 방법입니다.

```
> vi [filename].txt

push button i for edit

test

esc // 편집모드 나가기
:wq // 저장 후 닫기
```

### nano

`nano`의 경우도 vi와 같은 editor 입니다. 다만 저는 사용해본적은 없지만 굉장히 사용하기 편하다고 합니다. 

```
nano [filename].txt
```

이렇게 `bash`에서 파일을 만드는 4가지 방법에 대해서 알아보았습니다.

감사합니다.
