---
layout: base
published: false
title: "[Linux] Bash 디렉토리 전체 삭제 (Remove directory recursively in linux bash)"
image: /assets/img/blog/bash.png
description: >
  [Linux] Bash 디렉토리 전체 삭제 (Remove directory recursively in linux bash)
author: navy apple
comments: true
toc: true
permalink: /dev/linux/rm-recursive
tags:
  - bash
  - remove
  - directory
  - rmdir
  - rm
---

## [Linux] Bash 디렉토리 전체 삭제 (Remove directory recursively in linux bash)

Bash에서 디렉토리를 삭제하고 싶은 경우가 많이 있습니다. 디렉토리를 지우는 방법에는 두 가지가 있습니다.

> 디렉토리 삭제 (remove directory)

1. rmdir을 이용한 방법
2. rm을 이용한 방법

### 예제

1. rmdir

```
rmdir [directory name]
```

이런 식으로 디렉토리를 삭제할 수 있습니다. 단, `directory`가 비어있을 때만 가능합니다. directory 가 비어있지 않으면 
다음과 같은 에러가 발생합니다.

```
rmdir: [directory name]: Directory not empty
```

> 재귀적으로 디렉토리 삭제하기 (remove directory recursively)

비어 있지 않은 디렉토리를 전체 삭제하고 싶으면 다음과 같이 `rm` 명령어를 사용해야합니다.

```
rm -r [directory name]
```

- r: 하위에 있는 것들도 들도 재귀적으로 제거하겠다는 의미 입니다.
- f: 경고를 무시하고 강제로 지우겠다라는 의미 입니다.

하지만 `rm`의 옵션을 주지 않고, 실행할 경우

```
> rm [directory name]
```

`rm: [directory name]is a directory.`라는 에러 메시지를 받게 됩니다.

간단하게 리눅스에서 디렉토리를 삭제하는 방법에 대해서 알아보았습니다.

감사합니다.
