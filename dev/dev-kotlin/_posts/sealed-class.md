
```kotlin
sealed class Bank {
  abstract fun deposit()

  class BankOfAmerica: Bank() {
    override fun deposit() {}
  }
  class BankOfKorea: Bank() {
    override fun deposit() {}
  }

}

fun eval(bank: Bank) =
  when(bank) {
    is Bank.BankOfAmerica -> "Bank of America"
    is Bank.BankOfKorea -> "Bank of Korea"
  }
```