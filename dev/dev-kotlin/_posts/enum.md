```kotlin
enum class BankType(val displayName: String) {
  BANK_OF_AMERICA("Bank of America"),
  BANK_OF_KOREA("Bank of Korea");
}
```

```kotlin
enum class BankType(val displayName: String) {
  BANK_OF_AMERICA("Bank of America") {
    override fun deposit() {}
  },
  BANK_OF_KOREA("Bank of Korea") {
    override fun deposit() {}
  };

  abstract fun deposit()
}
```

sealed class와 비교