---
layout: base
published: false
title: "[HTML] 공백(스페이스, 빈칸) 넣는 방법"
image: /assets/img/blog/html.png
description: >
  html에서 공백(스페이스, 띄어쓰기)를 삽입하는 방법에 대해서 소개합니다.
author: navy apple
comments: true
toc: true
permalink: /dev/etc/html-space
tags:
  - html
  - space
  - 공백
  - 띄어쓰기
  - 빈칸
---

## [HTML] 공백(스페이스, 빈칸) 넣는 방법

웹 프론트엔드 개발을 하다보면, 뜻대로 글이 써지지 않는 경우가 있습니다. 그 중 가장 잊기 쉬운 것이 **공백(스페이스)**인데요. 오늘은 HTML에 공백문자를 삽입하여 페이지에 노출이 잘 되는지까지 확인해보는 시간을 가져보도록 하겠습니다.

### Non-breaking Space

`Non-breaking space` 직역하면, 줄바꿈 없는 공백이라는 뜻입니다. 이를 HTML 환경에서 축약해서 나타내는 문자는 `&nbsp;` 입니다.

예제를 통해서 한 번 자세하게 알아보도록 하겠습니다.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Document</title>
  </head>
  <body>
    <div>This is    Test.</div>
  </body>
</html>
```

위와 같이 `This is    Test.` **is**와 **Test.** 문자 사이에 4개의 공백을 채웠습니다.

![non-breaking-space1](/assets/img/blog/non-breaking-space1.png)

결과는 위와 같이 공백이 하나인 문자가 출력됩니다. 이번에는 공백 대신에 `&nbsp;`를 사용해서 문서를 작성해보겠습니다.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Document</title>
  </head>
  <body>
    <div>This is&nbsp;&nbsp;&nbsp;&nbsp;Test.</div>
  </body>
</html>
```
일반적으로 사용하는 공백 대신에 4개의 `&nbsp;`로 채웠습니다.

![non-breaking-space2](/assets/img/blog/non-breaking-space2.png)

4개의 공백만큼 잘 떨어져서 출력되는 것을 확인하실 수 있습니다.

### 맺음

간단하게 html에서 공백 삽입하는 방법에 대해서 알아보았습니다. 혹시 궁금한 점이나 이상한 점 있으면 댓글 부탁드리겠습니다.

감사합니다.
