---
layout: base
title: "[Js] setTimeout, setInterval, clearInterval"
image: /assets/img/blog/javascript.png
description: >
  [Js] setTimeout, setInterval, clearInterval
author: navy apple
comments: true
toc: true
permalink: /dev/etc/js-time-functions
tags:
  - js
  - setTimeout
  - setInterval
  - clearInterval
---

## [Js] setTimeout, setInterval, clearInterval

프론트엔드 개발을 할 때, 자주는 아니지만 종종 써야하는 함수들이 있습니다. 그 중에서도 `시간 이벤트 함수(Timing Event Functions)`에
대해서 알아보는 시간을 가져보겠습니다.

> Timing Event Functions

- **setTimeout**: 지정한 시간이 지난 후, 지정한 함수를 `단 한번`만 실행되도록 해주는 이벤트 함수 입니다.

- **setInterval**: 지정한 시간 마다, 지정한 함수를 계속해서 실행해주는 이벤트 함수 입니다.

- **clearInterval**: `setInterval`함수와 같이 사용되는 함수로, Interval 이벤트 함수를 정지시키는 이벤트 함수입니다.

### 사용 예제

- **setTimeout**

```js
setTimeout(callback, miliSeconds)
```

```js
setTimeout(
  function () {
    console.log('log를 찍습니다.');
  },
  1000
);

/*
1000ms 뒤에 [log를 찍습니다.]가 콘솔창에 찍힙니다.
*/
```

- **setInterval**

```js
setInterval(callback, miliSeconds interval)
```

```js
var intervalFunction = setInterval(
  function () {
    console.log('log를 찍습니다.');
  },
  2000
);

/*
2000ms 마다 [log를 찍습니다.]가 콘솔창에 찍힙니다.
*/
```

- **clearInterval**

```js
clearInterval(intervalEventFunction)
```

```js
clearInterval(intervalFunction);

/*
2000ms 마다 [log를 찍습니다.]가 콘솔창에 찍고 있던, 이벤트를 종료시킵니다.
*/
```

#### setInterval로 자기 자신을 종료 시키고 싶은 경우 (clear setInterval this)

```js
var count = 0;
var threshold = 5;
var intervalFunction2 = setInterval(
  function () {
    console.log('log를 찍습니다.');
    count += 1;
    if (count === threshold) {
      clearInterval(intervalFunction2)
    }
  },
  2000
);

/*
2000ms 마다 [log를 찍습니다.]가 콘솔창에 찍고, 5번 찍히고 종료합니다.
*/
```

### 언제 사용하면 좋을까요?

#### static한 데이터를 업데이트 하고 싶은 경우

주기적으로 바꿔줘야 하는 데이터가 있을 수 있는데요<br>
예를 들어 갤러리의 이미지, 광고 이미지 등을 업데이트 할 때 사용 할 수 있습니다.

#### 새로운 데이터 요청이 필요한 경우

지금이야 백엔드에서 `server push` 기능이 있어서 주기적으로 내려줘야하는 데이터나 업데이트 되어야하는 것들이 있을 때
요청 없이 내려줄 수 있습니다.

하지만 `server push`가 불가능한 경우도 있고 대부분이 프론트에서 요청하는 것은 간단한 요청일 때가 많기 때문에, 
`시간 이벤트 함수(Timing Event Functions)`를 이용하면 간단하게 서비스를 구현할 수 있습니다.

시간 이벤트 함수(Timing Event Functions)들에 대해서 알아보았습니다.

감사합니다.
