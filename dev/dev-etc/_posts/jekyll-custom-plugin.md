---
layout: base
title: How to use custom plugins with Github pages
image: /assets/img/blog/jekyll.png
description: >
  Plugin is not working in Github pages, How to use custom plugins with github pages
author: navy apple
comments: true
toc: true
permalink: /dev/etc/js-time-functions
tags:
  - github pages
  - jekyll
  - plugin
  - ignored
lang: en-US
---

## How to use custom plugins with Github pages

나는 jekyll 기반의 github page에 플러그인을 적용해야했습니다. 왜냐하면 나는 내 블로그에 AMP를 
적용하고 싶었습니다. 그래서 `amp-jekyll`이라는 플러그인을 github에서 발견했습니다.

> What is AMP?

AMP(Accelerated Mobile Pages)는 모바일 환경에서 빠르게 페이지를 렌더링할 수 있도록 페이지에 
사용할 수 있는 태그, css, js 등을 제약하는 프로젝트입니다.

> amp-jekyll

이 플러그인은 단순합니다. 내가 만든 마크다운을 jekyll이 html 파일로 파싱합니다. 그 이후에
amp-jekyll 플러그인이 img 태그들을 amp-img 태그로 변경해줍니다.

### Ignored custom plugins in Github pages

나는 플러그인을 사용하기 위해 나의 `_config.yml`에 다음과 같은 코드를 추가했습니다.

```yml
plugins
  - ...
  - jekyll-seo
  - amp-jekyll

```

하지만, 플러그인이 적용되지 않았습니다. 그 이유는 다음과 같습니다.

GitHub Pages is powered by Jekyll. All Pages sites are generated using the `--safe` option to disable plugins (with the exception of some `whitelisted plugins`) for security reasons. Unfortunately, this means your plugins won’t work if you’re deploying to GitHub Pages.

##### Ref

{% include base/components/link-box-custom-url.html title='Plugins' description='You have 3 options for installing plugins:' url='jekyllrb.com' internal_url='https://jekyllrb.com/docs/plugins/installation/' bg_img_url='/assets/img/blog/new-tokki.png' %}

#### Whitelisted plugins

{% include base/components/link-box-custom-url.html title='Dependency versions | GitHub Pages' description='GitHub Pages uses the following dependencies and versions' url='pages.github.com' internal_url='https://pages.github.com/versions/' bg_img_url='/assets/img/blog/new-tokki.png' %}

맞습니다, amp-jekyll은 whitelisted plugins에 포함되지 않기 때문에 무시된 것입니다. 하지만 포
기 할 수 없습니다. 그래서 저는 방법을 찾던 중 다음과 같은 문서를 발견했습니다.

문서를 요약하면

github pages는 설정에 따라 다르겠지만, master 브랜치로 웹사이트를 퍼블리싱합니다.
