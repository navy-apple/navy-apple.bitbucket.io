---
layout: base
title: vscode 코드 정렬 단축키 (shortcut to align code in vscode)
image: /assets/img/blog/vscode.png
description: >
  vscode 코드 정렬하기 (shortcut to align code in vscode)
author: navy apple
comments: true
toc: true
permalink: /dev/etc/algin-code-vscode
tags: 
  - vscode
  - code align
  - shortcut
  - 코드 정렬
  - 단축키
---

## vscode 코드 정렬하기 (shortcut to align code in vscode)

요즘에 프론트 엔드 개발자분들에게 가장 인기있는 에디터는 아무래도 `VS code`라고 생각을 하고 있습니다. 저도 프론트 엔드 개발을 
할 때는 주로 `vscode`로 개발을 하고 있습니다. 플러그인들도 많고 UI도 잘 되어 있고 다양한 장점을 가지고 있는 것 같습니다.

코드를 작성하다보면 코드를 정렬하고 싶은 욕구가 생기기 마련이죠. 나 그리고 같이 일하는 동료를 위해서라도 코드 정렬은 꼭 필요한 
기능인 것 같습니다.

### 코드 정렬 단축키 (shortcut to align code)

> 방법은 
1. 정렬할 코드 선택하기
2. 정렬하기

#### 정렬할 코드 선택하기 (select code for aligning)

- Windows: `Ctrl` + `A` (전체 선택)
- MacOS: `⌘` + `A` (전체 선택)

#### 정렬하기 (align code)

- Windows: `Ctrl` + `K` + `F`
- MacOS: `⌘` + `K` + `F`

**주의: 반드시, 순서대로 누르셔야 합니다.** 그 이유는 `Ctrl` or `⌘` + `F` 키가 찾기 기능이라 찾기 기능이 먼저 동작되어 버립니다.

간단하게, `VS code`에서 코드를 정렬하는 방법에 대해서 알아보았습니다.

감사합니다.
