---
layout: base
published: false
title: wget/curl로 구글 드라이브 파일 다운로드
image: /assets/img/blog/gdown-terminal.png
description: >
  터미널에서 구글 드라이브에 있는 파일을 다운로드하는 방법에 대해서 알아보도록 하겠습니다. (How to download Google Drive Files using wget or curl)
author: navy apple
comments: true
toc: true
permalink: /dev/etc/download-google-drive-file-in-terminal
tags:
  - wget
  - curl
  - google drive
  - download google drive
---

## wget/curl로 구글 드라이브 파일 다운로드

서버 측에서 외부 리소스를 다운받아야하는 경우가 있습니다. CLI 환경에서 외부 리소스를 다운 받는 방법에는 대표적으로 **wget**과 **curl** 이렇게 두 가지가 있습니다.

과거에는 wget과 curl을 이용하여 구글 드라이브에 있는 파일을 다운로드 받을 수 있었지만 구글 드라이브의 보안이 강화되면서 wget/curl을 이용해서 모두 다운로드를 할 수 없어졌습니다.

<hr>

> 다운로드 링크

보안적인 이슈로 공유링크를 통해서 바로 받을 수 없고, `docs.google.com` 도메인으로 우회해서 다운로드 해야합니다.

### 공유 링크 얻는 방법

![gdown1](/assets/img/blog/gdown1.png)

구글 드라이브 (google drive)에 들어가서 다운로드 받으려는 파일을 마우스 오른쪽 버튼을 클릭합니다.

![gdown2](/assets/img/blog/gdown2.png)

오른쪽 버튼을 클릭 후, **공유 가능한 링크 가져오기**를 클릭합니다.

![gdown3](/assets/img/blog/gdown3.png)

복사 버튼을 클릭하여 링크를 얻을 수 있습니다. 제가 얻은 링크는 다음과 같고, 이 링크에서 `d/`와 `/view` 사이에 있는 값이 파일 아이디(FILEID)입니다.

만약 하단에 `제한됨`으로 되어있다면, `링크가 있는 모든 사용자에게 공개`로 변경해주셔야합니다.

```bash
https://drive.google.com/file/d/1H4LzXZXmRHCjmkC0EH5eWHbkg1c0Wnwj/view?usp=sharing

# FILEID: 1H4LzXZXmRHCjmkC0EH5eWHbkg1c0Wnwj
```

{% include base/components/google/infeed-ad.html slot=site.data.ad.first.infeed %}

### wget

과거 wget으로 google drive 내의 파일을 다운로드 받는 명령어는 다음과 같습니다.

```bash
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id={FILEID}' -O {FILENAME}
```
초기에는 위와 같은 명령어로 다운로드를 할 수 있었지만, 보안이 한 층더 강화되면서 동작하지 않게 되었습니다. 이 후 wget 옵션에 쿠키를 설정해서 다운로드 할 수 있는 방법이 나오게 되었습니다.
아래 명령어 `{FILEID}`를 실제 파일 아이디로 대체해서 파일을 다운로드 받을 수 있습니다.

```bash
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id={FILEID}' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id={FILEID}" -O {FILENAME} && rm -rf /tmp/cookies.txt 
```

#### Example

![gdown-wget1](/assets/img/blog/gdown-wget1.png)

```bash
ls | grep abcd
```
이름에 abcd라는 문자열을 포함하는 파일을 찾습니다. 

![gdown-wget2](/assets/img/blog/gdown-wget2.png)

`{FILEID}`에 실제 파일 아이디를 대체하여 명령어를 실행해줍니다.

![gdown-wget3](/assets/img/blog/gdown-wget3.png)

다운로드 중인 것을 확인할 수 있습니다.

![gdown-wget4](/assets/img/blog/gdown-wget4.png)

다시 파일을 찾아보면 `abcd.txt`가 생긴 것을 확인할 수 있습니다.


### curl

curl의 경우도 wget과 비슷하게 실행할 수 있겠지만, 현재까지는 방법이 없는 것 같습니다. 추후에 curl로도 다운로드를 할 수 있게된다면 업데이트 하도록하겠습니다.

{% include base/components/google/infeed-ad.html slot=site.data.ad.second.infeed %}

### gdown.pl

{% include base/components/link-box-custom-url.html title='GitHub - circulosmeos/gdown.pl: Google Drive direct download of big files' description='Google Drive direct download of big files' url='github.com' internal_url='https://github.com/circulosmeos/gdown.pl' bg_img_url='/assets/img/blog/github.png' %}

`perl`이라는 언어로 작성된 구글 드라이브 다운로드 스크립트입니다. perl 통신을 통해서 다운로드 받을 수 있습니다.

![gdown-pl](/assets/img/blog/gdown-pl.png)

```bash
chmod u+x gdown.pl
```

```bash
./gdown.pl 'https://docs.google.com/uc?export=download&id=1H4LzXZXmRHCjmkC0EH5eWHbkg1c0Wnwj' abcd.txt
```

### 맺음

이렇게 해서 **wget**, **curl**, **gdown.pl** 세 가지 방법으로 구글 드라이브에 있는 파일을 다운로드 받는 방법에 대해서 알아보았습니다.

혹시나 궁금한 점이나 이상한 부분이 있으면 댓글 부탁드리겠습니다.

감사합니다.
