---
layout: base
title: process vs. thread (프로세스 vs. 쓰레드)
image: /assets/img/blog/process-thread.png
description: >
  프로세스와 스레드의 차이를 알아보자.
author: navy apple
comments: true
toc: true
permalink: /dev/etc/process-vs-thread
tags:
  - process
  - thread
  - operating system
  - 프로세스
---

## process vs. thread (프로세스 vs. 쓰레드)

프로세스와 쓰레드의 각각의 정의와 특성에 대해서 알아보도록 하겠습니다.

### 프로세스(Process)란?

````
In computing, a process is an instance of a computer program that is being executed. 
It contains the program code and its activity.

from wikipedia
````

발번역을 해보면

````
프로세스는 실행될 수 있는 프로그램의 인스턴스이고, 프로그램의 코드와 상태를 포함한다.
````

- 프로세스란 간단하게 프로그램이 메모리에 올라와 있는 상태로 실행중인 프로그램이라고 할 수 있습니다.

````
특징
1. 프로세스는 OS에 의해 scheduling의 대상이 되는 작업(Job)이라고 불린다.
2. OS는 PCB을 가지고 있고, 이 것으로 프로세스를 컨트롤 할 수 있다. (일부 운영 체제에서 PCB는 커널 스택의 처음에 위치한다.)
````

<p align="center">
<img src="https://steemitimages.com/DQmTYJS4cdwtSfVqtem9oRVgRb3Agsgq2mxbDUYMgyQEb5T/KakaoTalk_Photo_2018-03-27-15-35-42_84.png" alt="KakaoTalk_Photo_2018-03-27-15-35-42_84.png">
</p>

- Pointer: 프로세스의 메모리 상의 시작 주소를 가리키는 값.
- Process status: 프로세스의 상태 정보.
- Process number: 프로세스의 번호.
- Program counter: 프로세스의 다음 명령어를 저장.
- List of open file: 열린 파일들의 리스트.

즉, 프로세스는 위와같이 사용중인 메모리, 상태정보, 파일 등의 정보들을 포함하는 개념이다.

### 스레드(thread)란?

````
a thread of execution is the smallest sequence of programmed instructions that can be managed independently by a scheduler,
which is typically a part of the operating system.
The implementation of threads and processes differs between operating systems, but in most cases a thread is a component of a process.

from wikipedia
````
역시 발번역을 해보면

````
실행 스레드는 일반적으로 운영 체제의 일부인 스케줄러에 의해 독립적으로 관리 될 수있는 프로그래밍 된 명령어의 최소 시퀀스(흐름)이다.
스레드와 프로세스의 구현은 운영 체제마다 다르지만, 대부분의 경우 스레드는 프로세스의 구성 요소이다.
````

- 스레드는 하나의 프로세스 내에서 여러 개의 실행 흐름을 두기 위한 모델이다. 즉 하나 이상의 스레드가 모여 하나의 프로세스를 구성할 수 있다.
- 스레드는 리눅스에서 경량 프로세스라고 인식한다.
