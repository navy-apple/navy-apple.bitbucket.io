---
layout: base
published: false
title: "[Js] 중복 없이 배열 합치기 (Merge arrays without duplicates)"
image: /assets/img/blog/javascript.png
description: >
  javascript에서 값의 중복없이 배열을 합치는 방법에 대해서 알아봅니다.
author: navy apple
comments: true
toc: true
permalink: /dev/etc/js-merge-array-without-duplicates
tags:
  - js
  - concat
  - deduplicate
  - merge arrays
---

## 중복 없이 배열 합치기 (Merge arrays without duplicates)

자바스크립트 뿐만 아니라 여러가지 언어에서 개발을 하면서 여러 배열들을 병합해야하는 경우가 있습니다. 요즘 새로 생긴 언어같은 경우 간단하게 처리가 가능한 내장 함수를 지원해주기도 합니다. js에서는 중복 없이 배열을 합치는 함수는 없지만 여러가지 빌트인 함수를 이용하여 손쉽게 코드를 작성할 수 있습니다.

### Concat

`Array.prototype.concat()` 함수는 두 개 또는 그 이상의 배열들을 합치는 함수입니다.

#### example

```js
var a1 = [1, 2, 3];
var a2 = [3, 4, 5, 6];
var a3 = a1.concat(a2);

console.log(a3);
// expected output: Array [1, 2, 3, 3, 4, 5, 6]
```

##### Ref

{% include base/components/link-box-custom-url.html title='Array.prototype.concat() - JavaScript | MDN' description='The concat() method is used to merge two or more arrays. This method does not change the existing arrays, but instead returns a new array.' url='developer.mozilla.org' internal_url='https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat' bg_img_url='/assets/img/blog/mozilla.png' %}

### 중복 제거하기

위 `concat()` 함수를 통해서 얻어진 배열에서 중복을 제거해야하는 경우가 있는데요. 그런 경우 다음과 같이 `filter()` 함수를 이용하여 손쉽게 중복 값을 제거 할 수 있습니다.

```js
var a = [1, 2, 3, 3, 4, 5, 6];
var unique = a.filter((item, pos) => c.indexOf(item) === pos);

console.log(unique);
// [1, 2, 3, 4, 5, 6]
```

### 중복 없이 배열 합치기

결론적으로 두 가지 함수를 한 번에 적용하면 다음과 같이 간단하게 코드를 작성할 수 있습니다.

```js
var a
var a1 = [1, 2, 3];
var a2 = [3, 4, 5, 6];

var unique = a1.concat(a2).filter((item, pos) => c.indexOf(item) === pos);

console.log(unique);
// [1, 2, 3, 4, 5, 6]
```

### 맺음

간단하게 중복없이 배열을 합치는 방법에 대해서 알아보았습니다. 궁금하신 점이나 이상한 부분은 댓글로 남겨주세요

감사합니다.
