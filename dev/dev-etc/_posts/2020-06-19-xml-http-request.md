---
layout: base
published: false
title: XMLHttpRequest 예제 (XMLHttpRequest example)
image: /assets/img/blog/javascript.png
description: >
  XMLHttpRequest 예제 (XMLHttpRequest example)
author: navy apple
comments: true
toc: true
permalink: /dev/etc/xml-http-request
tags:
  - XMLHttpRequest
  - xhr
  - async
  - javascript
---

## XMLHttpRequest 예제 (XMLHttpRequest example)

 font-end 개발을 하다 보면, `javascript`를 이용해서 back-end로 http 요청을 보내야할 때가 있습니다. 그런 경우,
`XMLHttpRequest`를 이용하여 아주 간단하고 쉽게 요청을 보낼 수 있습니다.

> XMLHttpRequest란?

아래 링크 참조하시면 저의 설명보다 명확하게 설명이 되어 있습니다.

📎 [XMLHttpRequest - Web APIs: MDN](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest)

이제 예제에 대해서 알아보겠습니다.

### 예제 (Example)

```javascript
var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === xmlHttp.DONE) {
            if (xmlHttp.status === 200) {
              // do something with xmlHttp.responseText
            } else {
              // handle errors
            }
        }
    };
    xmlHttp.open( "GET", 'https://test.com');
    xmlHttp.send(null);
}
```

- Enums
  - UNSENT(0): XMLHttpRequest 객체의 생성된 상태를 말합니다.
  - OPEND(1): 예제에서도 있지만 `open(~)`메소드가 성공적으로 실행된 상태입니다.
  - HEADERS_RECEIVED(2): 모든 요청에 대한 응답이 도착한 상태입니다.
  - LOADING(3): 요청했던 데이터가 처리중(processing)인 상태입니다.
  - DONE(4): 데이터 처리가 완료(processed)된 상태입니다.
  

- Status: 상태값의 경우, 잘 아시는 Http 상태 코드 값과 동일하다고 생각하시면 됩니다.
  - 200: SUCCESS
  - 201: CREATED
  - 404: NOT FOUND
  - 나머지는 📎 [HTTP 상태 코드 - HTTP: MDN](https://developer.mozilla.org/ko/docs/Web/HTTP/Status)에서 확인하실 수 있습니다.
  

XMLHttpRequest 예제 대해서 알아보았습니다.<br>
감사합니다.
