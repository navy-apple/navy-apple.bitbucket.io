---
layout: base
title: multiple select key
image: /assets/img/blog/mybatis.png
description: >
  multiple select key
author: navy apple
comments: true
toc: true
permalink: /dev/etc/multiple-select-key
tags: 
  - mybatis
  - spring boot
  - select key
  - multiple column
---

## selectKey 컬럼 여러개 사용하기 (multiple select key)

Mybatis 3.2.6 버전부터 selectKey에 다음과 같이 여러 개의 컬럼을 가져올 수 있습니다.
방법은 아래의 `selectKey` 코드에 있습니다.

```sql
<insert id="insertTest">
        INSERT INTO test_table(
            user_id,
            user_name,
            column1,
            column2
        )
        VALUES (
            #{userId},
            #{userName},
            #{column1},
            #{column2}
        )
        ON DUPLICATE KEY UPDATE
            column1 = #{column1},
            column2 = #{column2}
        <selectKey keyColumn="id,user_name" keyProperty="id,userName" resultType="map" order="AFTER">
            SELECT
                id,
                user_name
            FROM test_table
            WHERE
              column1 = #{column1}
        </selectKey>
    </insert>
```
