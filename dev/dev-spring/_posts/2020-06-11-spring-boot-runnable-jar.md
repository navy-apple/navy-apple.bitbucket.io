---
layout: base
published: false
title: 스프링부트 jar[war] 실행하기 (Run spring boot runnable jar[war])
image: /assets/img/blog/springboot.png
description: >
  스프링부트 jar 실행하기.
author: navy apple
comments: true
toc: true
permalink: /dev/spring/boot-runnable-jar
tags: 
  - spring boot
  - jar
  - boot jar
  - war
  - boot war
  - gradle
  - runnable jar
  - runnable war
---

## 스프링부트 jar 실행하기 (Run spring boot runnable jar)

웹 개발을 하다보면 jar 파일을 서버에서 수동으로 동작시켜야하는 경우가 있습니다. 이런 경우에 사용할 수 있는
spring boot의 `bootWar`, `bootJar`설정 및 실행 방법에 대해서 알아보겠습니다.

### 스프링부트 내장 톰캣 (Springboot interal tomcat)

스프링부트를 사용하는 경우.

다음과 같은 gradle dependency를 추가할 수 있습니다.

```kotlin
implementation("org.springframework.boot:spring-boot-starter-web")
providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
```
이렇게 dependency를 추가할 경우, springboot 내장 톰캣을 사용할 수 있습니다.
따로 톰캣을 설치할 필요가 없어서 간편하게 어플리케이션을 구동시킬 수 있습니다.

### 실행하기 (run)

흔히들 많이 사용하는 intellij에서는 따로 설정을 해주지 않아도,spring initalizer를 통해
프로젝트를 생성했다면, 바로 실행버튼을 누르거나 실행 단축키를 누를 경우, 바로 내장 톰캣이 구동됩니다.

하지만 어플리케이션의 jar 또는 war로 결과물을 만들고, 이를 서버에서 실행시키기위해서는 다음과 같은 
추가적인 설정이 필요합니다.

### bootJar[bootWar] gradle.kts

spring boot에서는 boot에서 자동으로 설정하여 패키징해주는 태스크가 존재합니다.
gradle을 사용할 경우, 아래 그림처럼 gradle 작업창에서 [Tasks] -> [build] -> [bootJar] or [bootWar]를 선택하고,
명령어를 실행해줍니다.

![gradle](/assets/img/blog/gradle-setting.png)

실행을 하면, 아래와 같이 bootJar 또는 bootWar에 대한 설정이 추가되어 있는 것을 확인하실 수 있습니다.

```kotlin
plugins {
    id("org.springframework.boot") version "2.2.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    war                 // <- war로 패키징을 하는 경우.
    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
}

// bootJar
tasks.withType<org.springframework.boot.gradle.tasks.bundling.BootJar>().configureEach {
    launchScript()
}
// bootWar
tasks.withType<org.springframework.boot.gradle.tasks.bundling.BootWar>().configureEach {
    launchScript()
}
```

이후, `{project-path}/build/libs/`로 이동하시면, jar[war] 파일이 생성되어 있는 것을 확인하실 수 있습니다.

### 백그라운드로 jar[war] 실행하기.

요즘에는 AWS등 클라우드를 이용하여, 웹서버를 실행하는 경우가 많습니다. 하지만, 그냥 ssh 접근을 통해서,
jar[war]를 실행한 후, ssh 접속을 끊으면, 접속을 끊음과 동시에 실행했던 어플리케이션이 같이 종료되는 것을 확인하실 수 있습니다.

1. jdk 설치하기.

```
> yum install -y java-11-openjdk-devel
```

2. jar 실행하기.

```
> java –jar application.jar
```

3. background로 실행하기.

```
> nohup java –jar application.jar &
```

이렇게 실행해주시면 **nohub.out** 파일이 생성되는데 이 곳에 어플리케이션이 뜨면서 나오는 로그 등의 아웃풋들이 쓰여지게 됩니다.

4. application이 실행중인지 확인하기.

```
> ps -ef | grep java
[pid]xxxxx
```

5. application 종료하기.

```
kill -9 [pid]xxxxx
```
