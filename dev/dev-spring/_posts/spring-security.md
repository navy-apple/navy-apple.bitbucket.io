---
layout: post
title: 스프링 시큐리티 (Spring security) (1)
image: /assets/img/blog/springboot.png
description: >
  스프링 시큐리티 이해하기 (Understand Spring security)
author: navy apple
comments: true
toc: true
permalink: /dev/spring/security-1
tags: 
  - spring security
  - spring
  - security
  - spring boot
  - boot
---

## 스프링 시큐리티 (Spring security) (1)

최근들어 스프링 관련 모듈들에 대해서 공부를 하게되었습니다. 그리고 개인적으로 하고 있는 웹/앱 프로젝트가 있는데,
스프링 시큐리티 및 Oauth2.0을 적용하고 싶은 욕구가 생겼습니다.

그래서 오늘은 스프링 시큐리티에 대해서 정리해보려합니다.

> Spring Security란?

- spring 기반의 어플리케이션을 위한 선언적 보안 프레임워크
- 특징으로는 Servlet Filter 및 AOP를 기반으로한 보안 프레임워크

<hr>

### 지원하는 Authentication Models

Spring Security에서 지원하는 인증 모델들에 대해서 알아보겠습니다.

1. HTTP BASIC authentication headers
2. HTTP Digest authentication headers
3. HTTP X.509 client certificate exchange
4. Form-based authentication
5. LDAP
6. CAS (Jasig Central authentication service)
7. Authentication based on pre-established reqeuest headers
8. Kerberos
9. OpenID authentication

<hr>

### Spring Security Modules

Spring Security 관련된 모듈들에 대해서 정리해보면 다음과 같이 상당히 다양하게 있습니다.

#### **Basic**
- **spring-security-core**: authentication, access-cotrol
- **spring-security-config**: XML namespace configuration include security XML namespace xsd files, Java configuration
- **spring-security-web**: filters, web security infrastucture

#### **Adveaced**
- **spring-security-acl**: domain object ACL
- **spring-security-remoting**: provides integration with Spring Remoting
- **spring-security-test**: support for testing with Spring Security

#### **Auth**
- **spring-security-ldap** 
- **spring-security-cas**
- **spring-security-openid**

#### Spring Security 5 **OAuth 2.0**
- **spring-security-oauth2-core**
- **spring-security-oauth2-client**: include OAuth 2.0 Login
- **spring-security-oauth2-jose**: support JWT, JWS, JWE, JWK

#### **Extensions**
- **spring-security-kerberos-web**
- **spring-security-saml2-core**
- **spring-security-oauth2**: Spring Security 5이전의 모듈, 이 것으로 OAuth2 Authorization Server를 구현할 수 있습니다.

여기까지 `Spring Security`의 지원 인증모델, 모듈들의 종류들에 대해서 알아보았습니다.

출처: [Spring Security](https://spring.io/projects/spring-security)

<hr>

### 보안 기본 개념

#### **인증(Authetication)** / **인가(Athorization)**

> 인증과 인가란?

- **인증(Authetication)**이란 자기 자신을 누구라고 주장하는 `주체(Principal)`를 확인하는 프로세스
- **인가(Athorization)**이란 어플리케이션 `주체(Principal)`이 어떤 행위를 수행하도록 허락되었는지 여부를 결정하는 프로세스

일반적으로,

```
Authetication -> Athorization
```
인증이된 이후에 인가를 받아 어떤 어플리케이션에서
어떤 행위를 수행할 수 있습니다.

<hr>

### Spring Security 예제 프로젝트

#### 개발 환경
- IDE: Itellij IDEA Ultimate 2018.03
- Java: JDK 11

1. 프로젝트 생성하기

![springinit](https://user-images.githubusercontent.com/61483154/76084696-d09f1680-5ff3-11ea-92e8-9052b4f8f55b.png)

- [File] -> [new] -> [Project] 스텝으로 새로운 프로젝트 만들기를 선택합니다.
- 위 그림과 같이 `Spring Initalizer`를 선택합니다.

![springsetproj](https://user-images.githubusercontent.com/61483154/76084748-fcba9780-5ff3-11ea-8034-869501236afa.png)

- group, atifect를 개인에 맞게 작성합니다.
- type은 `gradle project`를 선택합니다.
- 패키징은 `War`를 선택합니다.
- 자바 버전은 `jdk11`을 선택합니다.

![sprinsetmodu](https://user-images.githubusercontent.com/61483154/76084755-ffb58800-5ff3-11ea-8e3b-ee0baa2233b7.png)

- [Web] -> [Spring Web] 추가합니다.
- [Security] -> [Spring Security] 추가합니다.

![springsetname](https://user-images.githubusercontent.com/61483154/76084764-017f4b80-5ff4-11ea-88bc-d351970a1a04.png)

- 프로젝트 이름을 정하고, `Finish`를 클릭하고 프로젝트 생성을 완료합니다.

![gradleset](https://user-images.githubusercontent.com/61483154/76085458-6d15e880-5ff5-11ea-8418-35fb9a22c75e.png)

- 프로젝트가 생성됨과 동시에 Gradle 세팅을 하라고 얼럿창이 뜨는데, `Auto-import` 기능을 체크하고 `OK`를 누르면 자동으로 프로젝트가 세팅됩니다.

#### **build.gradle**

Gradle 세팅이 완료되면, `build.gradle`를 확인할 수 있는데요. 다음과 같이 잘 생성이 되었습니다.

```gradle
plugins {
    id 'org.springframework.boot' version '2.2.5.RELEASE'
    id 'io.spring.dependency-management' version '1.0.9.RELEASE'
    id 'java'
    id 'war'
}

group = 'com.example.security'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

repositories {
    mavenCentral()
}

dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-security'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    testImplementation('org.springframework.boot:spring-boot-starter-test') {
        exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
    }
    testImplementation 'org.springframework.security:spring-security-test'
}

test {
    useJUnitPlatform()
}

```

<hr>

Spring Security를 설정하는 방법에는 XML Config와 Java Config 두 가지 방법으로 할 수 있습니다.
두 가지 모두 예제 코드를 작성해보겠습니다.

### Security XML 설정(Configuration)

![springcontext2](https://user-images.githubusercontent.com/61483154/76086666-e3b3e580-5ff7-11ea-9409-7b5215021a9d.png)

디렉토리 구조는 위와 같이 되어 있습니다. 이 상태에서 [resources] 밑에 `spring`이라는 디렉토리를 생성하고, 아래 두 가지 xml 파일을
생성해줍니다.

- `root-context.xml`
- `security.xml`


#### **root-context.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
       
    <import resource="security.xml" />
    
</beans>
```

#### **security.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans:beans xmlns="http://www.springframework.org/schema/security"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xmlns:beans="http://www.springframework.org/schema/beans"
             xsi:schemaLocation="http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security.xsd
                                 http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    
    <http use-expressions="true">
        <intercept-url pattern="/**"                 access="permitAll()" />
        <intercept-url pattern="/project/**"         access="isAuthenticated()" />
        <intercept-url pattern="/redirect-index"     access="isAuthenticated()" />
        <intercept-url pattern="/private-project/**" access="hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')" />
        <intercept-url pattern="/admin/**"           access="hasAuthority('ROLE_ADMIN')" />

        <csrf disabled="true" />
        <form-login />
        <logout />
    </http>

    <authentication-manager>
        <authentication-provider>
            <user-service>
                <user name="guest"  password="{noop}guest" authorities="ROLE_GUEST" />
                <user name="user" password="{noop}user" authorities="ROLE_USER" />
                <user name="admin"  password="{noop}admin" authorities="ROLE_ADMIN" />
            </user-service>
        </authentication-provider>
    </authentication-manager>

</beans:beans>
```


