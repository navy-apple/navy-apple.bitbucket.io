require 'nokogiri'

module Jekyll
  module ReactiveImageFilter
    # Filter for HTML 'img' elements.
    # Converts elements to 'reactive-img' and adds additional attributes
    # Parameters:
    #   input       - the content of the post
    #   responsive  - boolean, whether to add layout=responsive, true by default
    def reactive_images(input)
      doc = Nokogiri::HTML.fragment(input);
      # Change 'img' elements to 'reactive-img', add responsive attribute when needed
      doc.css('img:not([width])').each do |image|
        if image['src'].start_with?('http://', 'https://')
          src = image['src']
        else
          # FastImage doesn't seem to handle local paths when used with Jekyll
          # so let's just force the path
          src = File.join(Dir.pwd, '', image['src'])
        end
        # Jekyll generates static assets after the build process.
        # This causes problems when trying to determine the dimensions of a locally stored image.
        # For now, the only solution is to skip the build and generate the AMP files after the site has beem successfully built.
        # TODO: find a better solution.
        begin
          size = FastImage.size(src)
          image['ratio'] = (size[1].to_f/size[0]).round(2) * 100
        rescue Exception => e
          puts 'Unable to get image dimensions for "' + src + '". For local files, build the site with \'--skip-initial-build\' for better results. [Error: ' + e.to_s + ']'
        end
      end
      doc.css('img').each do |image|
        image.name = "reactive-img"
      end
      # Added <img /> tag wrapped with <noscript /> in case js is not enabled
      # but image will still show up. The element would look like this:
      # <reactive-img ...>
      #    <noscript>
      #        <img ... />
      #    </noscript>
      # </reactive-img ...>
      # Duplicate reactive-img, remove layout attribut, wrap it with noscript, and add
      # it as reactive-img child
      doc.css('reactive-img').each do |reactive_img|
        noscript = Nokogiri::XML::Node.new "noscript", doc
        
        loading = Nokogiri::XML::Node.new "span", doc
        loading['class'] = "loading"
        cog = Nokogiri::XML::Node.new "svg", doc
        cog['class'] = "gear"
        cog['xmlns'] = "http://www.w3.org/2000/svg"
        cog['width'] = "24"
        cog['height'] = "24"
        cog['viewBox'] = "0 0 24 24"
        cog_path = Nokogiri::XML::Node.new "path", doc
        cog_path['fill'] = "#bbb"
        cog_path['d'] = "M24 13.616v-3.232c-1.651-.587-2.694-.752-3.219-2.019v-.001c-.527-1.271.1-2.134.847-3.707l-2.285-2.285c-1.561.742-2.433 1.375-3.707.847h-.001c-1.269-.526-1.435-1.576-2.019-3.219h-3.232c-.582 1.635-.749 2.692-2.019 3.219h-.001c-1.271.528-2.132-.098-3.707-.847l-2.285 2.285c.745 1.568 1.375 2.434.847 3.707-.527 1.271-1.584 1.438-3.219 2.02v3.232c1.632.58 2.692.749 3.219 2.019.53 1.282-.114 2.166-.847 3.707l2.285 2.286c1.562-.743 2.434-1.375 3.707-.847h.001c1.27.526 1.436 1.579 2.019 3.219h3.232c.582-1.636.75-2.69 2.027-3.222h.001c1.262-.524 2.12.101 3.698.851l2.285-2.286c-.744-1.563-1.375-2.433-.848-3.706.527-1.271 1.588-1.44 3.221-2.021zm-12 2.384c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"
        cog.add_child(cog_path)
        loading.add_child(cog)

        if reactive_img['src'].start_with?('http://', 'https://')
        else 
          reactive_img['alternative-src'] = reactive_img['src']
          reactive_img['src'] = reactive_img['src'].gsub(/\.(gif|jpe?g|tiff|png|webp|bmp)$/i, '.webp')
        end

        noscript_img = reactive_img.dup
        noscript_img.name = 'img'

        noscript.add_child(noscript_img)
        reactive_img['style'] = "width:100%;padding-top: #{reactive_img['ratio']}%;"
        reactive_img.add_child(noscript)
        reactive_img.add_child(loading)
      end
      doc.to_s
    end
  end
end

Liquid::Template.register_filter(Jekyll::ReactiveImageFilter)