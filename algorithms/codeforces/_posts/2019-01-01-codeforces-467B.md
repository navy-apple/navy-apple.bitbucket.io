---
layout: base
title: 코드포스 467B (codeforces 467B)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 467B (codeforces 467B) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/467B
tags:
  - codeforces
  - codeforces 467B
  - 코드포스
  - 코드포스 467B
  - Problem - 467B - Codeforces
---

## 코드포스 467B (codeforces 467B)
Problem - 467B - Codeforces

{% assign title = 'Codeforces 467B' %}
{% assign description = '코드포스 467B (codeforces 467B) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/467/B' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF467B {
    static int n, m, k, p[] = new int[1001], f, ans = 0;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        n = Integer.parseInt(ins[0]);
        m = Integer.parseInt(ins[1]);
        k = Integer.parseInt(ins[2]);
        for (int i = 0; i < m; i++) {
            p[i] = Integer.parseInt(br.readLine());
        }
        f = Integer.parseInt(br.readLine());

        for (int i = 0; i < m; i++) {
            String t = Integer.toBinaryString(f ^ p[i]);
            t = t.replace("0", "");
            ans += (t.length() <= k) ? 1 : 0;
        }
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명