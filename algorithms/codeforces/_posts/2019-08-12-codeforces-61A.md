---
layout: base
title: 코드포스 61A (codeforces 61A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 61A (codeforces 61A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/61A
tags:
  - codeforces
  - codeforces 61A
  - 코드포스
  - 코드포스 61A
  - Problem - 61A - Codeforces
---

## 코드포스 61A (codeforces 61A)
Problem - 61A - Codeforces

{% assign title = 'Codeforces 61A' %}
{% assign description = '코드포스 61A (codeforces 61A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/61/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF61A {
    static String s1, s2, s;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        s1 = br.readLine();s2 =br.readLine();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s1.length(); i++) {
            sb.append("" + ((s1.charAt(i) - 48) ^ (s2.charAt(i) - 48)));
        }
        bw.write(sb.toString() + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명