---
layout: base
title: 코드포스 7A (codeforces 7A)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 7A (codeforces 7A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/7A
tags:
  - codeforces
  - codeforces 7A
  - 코드포스
  - 코드포스 7A
  - Problem - 7A - Codeforces
---

## 코드포스 7A (codeforces 7A)
Problem - 7A - Codeforces

{% assign title = 'Codeforces 7A' %}
{% assign description = '코드포스 7A (codeforces 7A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/7/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF7A {
    static int ans = 0;
    static char[][] table = new char[9][9];
    static boolean row[] = new boolean[9];
    static boolean col[] = new boolean[9];
    static boolean isBlack;
    static String in;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        for (int i = 1; i < 9; i++) {
            in = br.readLine();isBlack = true;
            for (int j = 1; j < 9; j++) {
                table[i][j] = in.charAt(j - 1);
                if (isBlack) isBlack = (table[i][j] == 'B');
            }
            row[i] = isBlack;
            if (row[i]) {
                ans++;
                for (int j = 1; j < 9; j++) {
                    table[i][j] = 'W';
                }
            }
        }
        for (int j = 1; j < 9; j++) {
            isBlack = false;
            for (int i = 1; i < 9; i++) {
                if (table[i][j] == 'B') {
                    isBlack = true;break;
                }
            }
            col[j] = isBlack;
            if (col[j]) {
                ans++;
                for (int i = 1; i < 9; i++) {
                    table[i][j] = 'W';
                }
            }
        }




        bw.write(ans + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명