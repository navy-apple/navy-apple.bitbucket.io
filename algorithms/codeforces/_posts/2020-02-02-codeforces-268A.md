---
layout: base
title: 코드포스 268A (codeforces 268A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 268A (codeforces 268A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/268A
tags:
  - codeforces
  - codeforces 268A
  - 코드포스
  - 코드포스 268A
  - Problem - 268A - Codeforces
---

## 코드포스 268A (codeforces 268A)
Problem - 268A - Codeforces

{% assign title = 'Codeforces 268A' %}
{% assign description = '코드포스 268A (codeforces 268A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/268/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF268A {
    static int n, a[] = new int[31], h[] = new int[31];
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        for (int i = 1; i <= n; i++) {
            ins = br.readLine().split(" ");
            h[i] = Integer.parseInt(ins[0]);
            a[i] = Integer.parseInt(ins[1]);
        }
        int count = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i == j) continue;
                if (h[i] == a[j]) count++;
            }
        }
        bw.write(count + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명