---
layout: base
title: 코드포스 22B (codeforces 22B)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 22B (codeforces 22B) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/22B
tags:
  - codeforces
  - codeforces 22B
  - 코드포스
  - 코드포스 22B
  - Problem - 22B - Codeforces
---

## 코드포스 22B (codeforces 22B)
Problem - 22B - Codeforces

{% assign title = 'Codeforces 22B' %}
{% assign description = '코드포스 22B (codeforces 22B) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/22/B' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF22B {
    static int n, m, l[], r[], max = -1;
    static String ins[], in;
    static boolean[][] table = new boolean[26][26];
    static int[] col = new int[26];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        n = Integer.parseInt(ins[0]);
        m = Integer.parseInt(ins[1]);
        for (int i = 0; i < n; i++) {
            in = br.readLine();
            for (int j = 0; j < m; j++) {
                table[i][j] = (in.charAt(j) == '1');
            }
        }

        for (int i = 0; i < n; i++) {
           for (int j = 0; j < m; j++) {
               int r = i, count = 0;
               while (true) {
                   if (!table[r][j]) {
                       count++; r++;
                       if (r == n) break;
                   }else break;
               }
               col[j] = count;
           }
           max = Math.max(max, getMaxPerimeter());
        }
        bw.write(max + "\n");
        bw.flush();bw.close();
    }

    public static int getMaxPerimeter() {
        int maxArea = -1;
        for (int i = 0; i < m; i++) {
            int h = 26, w = 0;
            if (col[i] == 0) continue;
            for (int j = i; j < m; j++) {
                if (col[j] == 0) break;
                w = j - i + 1;
                h = (h > col[j]) ? col[j] : h;
            }
            maxArea = Math.max(maxArea, 2 * (w + h));
        }
        return maxArea;
    }
}
```


### 설명