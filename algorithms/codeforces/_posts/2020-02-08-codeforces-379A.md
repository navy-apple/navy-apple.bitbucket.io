---
layout: base
title: 코드포스 379A (codeforces 379A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 379A (codeforces 379A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/379A
tags:
  - codeforces
  - codeforces 379A
  - 코드포스
  - 코드포스 379A
  - Problem - 379A - Codeforces
---

## 코드포스 379A (codeforces 379A)
Problem - 379A - Codeforces

{% assign title = 'Codeforces 379A' %}
{% assign description = '코드포스 379A (codeforces 379A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/379/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF379A {
    static int a, b, ans, remain;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        ans = remain = a = Integer.parseInt(ins[0]);
        b = Integer.parseInt(ins[1]);
        while ((remain / b) > 0){
            int t = remain % b;
            remain /= b;
            ans += remain;
            remain += t;
        }
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명