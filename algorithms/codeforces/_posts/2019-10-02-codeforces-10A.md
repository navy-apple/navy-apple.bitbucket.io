---
layout: base
title: 코드포스 10A (codeforces 10A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 10A (codeforces 10A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/10A
tags:
  - codeforces
  - codeforces 10A
  - 코드포스
  - 코드포스 10A
  - Problem - 10A - Codeforces
---

## 코드포스 10A (codeforces 10A)
Problem - 10A - Codeforces

{% assign title = 'Codeforces 10A' %}
{% assign description = '코드포스 10A (codeforces 10A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/10/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF10A {
    static String[] ins;
    static int N, s, e;
    static long ans = 0;
    static int[] p = new int[3];
    static int[] t = new int[2];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        N = Integer.parseInt(ins[0]);
        for (int i = 1; i < 6; i++) {
            if (i < 4) p[i - 1] = Integer.parseInt(ins[i]);
            else t[i - 4] = Integer.parseInt(ins[i]);
        }
        int pre = -1, term;
        for (int i = 0; i < N; i++) {
            ins = br.readLine().split(" ");
            s = Integer.parseInt(ins[0]);
            e = Integer.parseInt(ins[1]);
            if (pre < 0) pre = s;
            ans += (e - s) * p[0];
            term = s - pre;
            if (term > t[0] + t[1]) {
                ans += (term - (t[0] + t[1])) * p[2];
                term = t[0] + t[1];
            }
            if (term > t[0]) {
                ans += (term - t[0]) * p[1];
                term = t[0];
            }
            ans += term * p[0];
            pre = e;
        }
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명