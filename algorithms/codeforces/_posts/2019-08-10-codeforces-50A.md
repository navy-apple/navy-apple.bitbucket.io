---
layout: base
title: 코드포스 50A (codeforces 50A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 50A (codeforces 50A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/50A
tags:
  - codeforces
  - codeforces 50A
  - 코드포스
  - 코드포스 50A
  - Problem - 50A - Codeforces
---

## 코드포스 50A (codeforces 50A)
Problem - 50A - Codeforces

{% assign title = 'Codeforces 50A' %}
{% assign description = '코드포스 50A (codeforces 50A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/50/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF50A {
    static int n, m, ans = 0;
    static String[] ins;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        n = Integer.parseInt(ins[0]);m = Integer.parseInt(ins[1]);
        ans = (n % 2 == 0) ? (n >> 1) * m : ((n - 1) >> 1) * m + (m >> 1);
        bw.write(ans + "\n");bw.flush();bw.close();
    }
}
```


### 설명