---
layout: base
title: 코드포스 9A (codeforces 9A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 9A (codeforces 9A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/9A
tags:
  - codeforces
  - codeforces 9A
  - 코드포스
  - 코드포스 9A
  - Problem - 9A - Codeforces
---

## 코드포스 9A (codeforces 9A)
Problem - 9A - Codeforces

{% assign title = 'Codeforces 9A' %}
{% assign description = '코드포스 9A (codeforces 9A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/9/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF9A {
    static int D, max, b = 6;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        max = 7 - Math.max(Integer.parseInt(ins[0]), Integer.parseInt(ins[1]));
        if (max % 2 == 0) {
            max >>= 1;b >>= 1;
        }
        if (max % 3 == 0) {
            max /= 3; b /= 3;
        }
        bw.write(max + "/" + b + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명