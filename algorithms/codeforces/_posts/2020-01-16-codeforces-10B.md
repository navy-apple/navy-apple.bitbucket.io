---
layout: base
title: 코드포스 10B (codeforces 10B)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 10B (codeforces 10B) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/10B
tags:
  - codeforces
  - codeforces 10B
  - 코드포스
  - 코드포스 10B
  - Problem - 10B - Codeforces
---

## 코드포스 10B (codeforces 10B)
Problem - 10B - Codeforces

{% assign title = 'Codeforces 10B' %}
{% assign description = '코드포스 10B (codeforces 10B) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/10/B' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class CF10B {
    static int N, K, cx, cy;
    static int[] m = new int[1001];
    static boolean[][] table = new boolean[100][100];
    static String[] ins;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        N = Integer.parseInt(ins[0]);
        K = Integer.parseInt(ins[1]);
        ins = br.readLine().split(" ");
        cx = cy = (K % 2 == 0) ? K / 2 : K / 2 + 1;
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= N; i++) {
            m[i] = Integer.parseInt(ins[i - 1]);
            int min = Integer.MAX_VALUE;
            Point p = new Point();
            for (int x = 1; x <= K; x++) {
                for (int y = 1; y <= K - m[i] + 1; y++) {
                    int abDistance = 0;
                    boolean flag = true;
                    for (int j = y; j < y + m[i]; j++) {
                        if (table[x][j]) {
                            flag = false;break;
                        }
                        abDistance += absoluteD(x, j);
                    }
                    if (flag && min > abDistance) {
                        min = abDistance;
                        p.x = x;p.y = y;
                    }
                }
            }
            Arrays.fill(table[p.x], p.y, p.y + m[i], true);
            sb.append(((p.x == 0) ? -1 : p.x + " " + p.y + " " + (p.y + m[i] - 1)) + "\n");
        }
        bw.write(sb.toString());
        bw.flush();bw.close();

    }
    public static int absoluteD(int x, int y) {
        return Math.abs(x - cx) + Math.abs(y - cy);
    }

    private static class Point {
        int x, y;
        Point() { x = 0; y = 0; }
        Point(int x, int y) {
            this.x = x; this.y = y;
        }
    }
}
```


### 설명