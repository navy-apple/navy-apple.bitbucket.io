---
layout: base
title: 코드포스 25A (codeforces 25A)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 25A (codeforces 25A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/25A
tags:
  - codeforces
  - codeforces 25A
  - 코드포스
  - 코드포스 25A
  - Problem - 25A - Codeforces
---

## 코드포스 25A (codeforces 25A)
Problem - 25A - Codeforces

{% assign title = 'Codeforces 25A' %}
{% assign description = '코드포스 25A (codeforces 25A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/25/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF25A {
    static int n, even = 0, odd = 0;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        ins = br.readLine().split(" ");
        for (int i = 1; i <= n; i++) {
            if (Integer.parseInt(ins[i - 1]) % 2 == 0) even += i;
            else odd += i;
        }

        bw.write(Math.min(even, odd) + "\n");
        bw.flush();bw.close();

    }
}
```


### 설명