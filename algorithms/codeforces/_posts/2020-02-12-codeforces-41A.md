---
layout: base
title: 코드포스 41A (codeforces 41A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 41A (codeforces 41A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/41A
tags:
  - codeforces
  - codeforces 41A
  - 코드포스
  - 코드포스 41A
  - Problem - 41A - Codeforces
---

## 코드포스 41A (codeforces 41A)
Problem - 41A - Codeforces

{% assign title = 'Codeforces 41A' %}
{% assign description = '코드포스 41A (codeforces 41A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/41/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF41A {
    static StringBuffer s1 = new StringBuffer(), s2 = new StringBuffer();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        s1.append(br.readLine());s2.append(br.readLine());
        bw.write((s1.reverse().toString().equals(s2.toString())) ? "YES\n" : "NO\n");
        bw.flush();bw.close();
    }
}
```


### 설명