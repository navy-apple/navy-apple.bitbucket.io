---
layout: base
title: 코드포스 13A (codeforces 13A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 13A (codeforces 13A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/13A
tags:
  - codeforces
  - codeforces 13A
  - 코드포스
  - 코드포스 13A
  - Problem - 13A - Codeforces
---

## 코드포스 13A (codeforces 13A)
Problem - 13A - Codeforces

{% assign title = 'Codeforces 13A' %}
{% assign description = '코드포스 13A (codeforces 13A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/13/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF13A {
    static int A;
    static long sum;
    static String hex;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        A = Integer.parseInt(br.readLine());
        for (int i = 2; i < A; i++) {
            sum += sumOfDecimalToBaseN(i, A);
        }
        int b = A - 2;
        long g = gcd(sum, b);
        sum /= g; b /= g;


        bw.write(sum + "/" + b + "\n");
        bw.flush();bw.close();
    }

    public static long gcd(long a, long b) {
        while (b != 0) {
            long tmp = a % b;
            a = b;
            b = tmp;
        }
        return Math.abs(a);
    }

    public static long sumOfDecimalToBaseN(int n, int a) {
        long ans = 0;
        while (a != 0) {
            int r = a % n;
            ans += r;
            a /= n;
        }
        return ans;
    }

}
```


### 설명