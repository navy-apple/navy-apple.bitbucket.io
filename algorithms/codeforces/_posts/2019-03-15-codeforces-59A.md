---
layout: base
title: 코드포스 59A (codeforces 59A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 59A (codeforces 59A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/59A
tags:
  - codeforces
  - codeforces 59A
  - 코드포스
  - 코드포스 59A
  - Problem - 59A - Codeforces
---

## 코드포스 59A (codeforces 59A)
Problem - 59A - Codeforces

{% assign title = 'Codeforces 59A' %}
{% assign description = '코드포스 59A (codeforces 59A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/59/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF59A {
    static String in;
    static int upper = 0, lower = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();
        for (int i = 0; i < in.length(); i++) {
            upper += (in.charAt(i) < 96) ? 1 : 0;
            lower += (in.charAt(i) > 96) ? 1 : 0;
        }
        bw.write(((upper > lower) ? in.toUpperCase() : in.toLowerCase()) + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명