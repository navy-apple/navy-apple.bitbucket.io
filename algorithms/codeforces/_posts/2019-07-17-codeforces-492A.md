---
layout: base
title: 코드포스 492A (codeforces 492A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 492A (codeforces 492A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/492A
tags:
  - codeforces
  - codeforces 492A
  - 코드포스
  - 코드포스 492A
  - Problem - 492A - Codeforces
---

## 코드포스 492A (codeforces 492A)
Problem - 492A - Codeforces

{% assign title = 'Codeforces 492A' %}
{% assign description = '코드포스 492A (codeforces 492A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/492/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF492A {
    static int n, ans = 0, j = 1, count = -1;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        for (int i = 2; ans <= n ; i++) {
            ans += j;j += i;
            count++;
        }
        bw.write(count + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명