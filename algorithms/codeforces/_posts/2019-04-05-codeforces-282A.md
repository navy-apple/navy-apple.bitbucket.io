---
layout: base
title: 코드포스 282A (codeforces 282A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 282A (codeforces 282A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/282A
tags:
  - codeforces
  - codeforces 282A
  - 코드포스
  - 코드포스 282A
  - Problem - 282A - Codeforces
---

## 코드포스 282A (codeforces 282A)
Problem - 282A - Codeforces

{% assign title = 'Codeforces 282A' %}
{% assign description = '코드포스 282A (codeforces 282A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/282/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF282A {
    static int n, ans = 0;
    static String in;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            in = br.readLine().replace("X", "");
            ans += (in.equals("++")) ? 1 : -1;
        }
        bw.write(ans + "\n");bw.flush();bw.close();
    }
}
```


### 설명