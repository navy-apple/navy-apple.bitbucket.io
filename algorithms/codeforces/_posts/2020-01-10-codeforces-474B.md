---
layout: base
title: 코드포스 474B (codeforces 474B)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 474B (codeforces 474B) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/474B
tags:
  - codeforces
  - codeforces 474B
  - 코드포스
  - 코드포스 474B
  - Problem - 474B - Codeforces
---

## 코드포스 474B (codeforces 474B)
Problem - 474B - Codeforces

{% assign title = 'Codeforces 474B' %}
{% assign description = '코드포스 474B (codeforces 474B) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/474/B' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF474B {
    static int n, m, a = 0, q, pile[] = new int[1000001];
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        ins = br.readLine().split(" ");
        for (int i = 1; i <= n; i++) {
            int b = a;
            a += Integer.parseInt(ins[i - 1]);
            for (int j = b + 1; j <= a; j++) {
                pile[j] = i;
            }
        }
        m = Integer.parseInt(br.readLine());
        StringBuffer sb = new StringBuffer();
        ins = br.readLine().split(" ");
        for (int i = 1; i <= m; i++) {
            q = Integer.parseInt(ins[i - 1]);
            sb.append(pile[q]);sb.append("\n");
        }
        bw.write(sb.toString());
        bw.flush();bw.close();
    }
}
```


### 설명