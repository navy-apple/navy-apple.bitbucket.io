---
layout: base
title: 코드포스 11A (codeforces 11A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 11A (codeforces 11A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/11A
tags:
  - codeforces
  - codeforces 11A
  - 코드포스
  - 코드포스 11A
  - Problem - 11A - Codeforces
---

## 코드포스 11A (codeforces 11A)
Problem - 11A - Codeforces

{% assign title = 'Codeforces 11A' %}
{% assign description = '코드포스 11A (codeforces 11A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/11/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF11A {
    static int n, d, count = 0;
    static String[] in;
    static int[] b = new int[2001];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine().split(" ");
        n = Integer.parseInt(in[0]);
        d = Integer.parseInt(in[1]);
        in = br.readLine().split(" ");
        for (int i = 0; i < n; i++) {
            b[i] = Integer.parseInt(in[i]);
        }
        for (int i = 1; i < n; i++) {
            if (b[i - 1] >= b[i]) {
                int t = b[i - 1] - b[i];
                int s = t / d + 1;
                b[i] += s * d;
                count += s;
            }
        }
        bw.write(count + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명