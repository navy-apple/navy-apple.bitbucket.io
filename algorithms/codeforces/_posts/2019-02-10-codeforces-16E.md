---
layout: base
title: 코드포스 16E (codeforces 16E)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 16E (codeforces 16E) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/16E
tags:
  - codeforces
  - codeforces 16E
  - 코드포스
  - 코드포스 16E
  - Problem - 16E - Codeforces
---

## 코드포스 16E (codeforces 16E)
Problem - 16E - Codeforces

{% assign title = 'Codeforces 16E' %}
{% assign description = '코드포스 16E (codeforces 16E) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/16/E' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF16E {
    static int n;
    static double[][] p = new double[19][19];
    static String[] ins;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            ins = br.readLine().split(" ");
            for (int j = 0; j < n; j++) {
                p[i][j] = Double.parseDouble(ins[j]);
            }
        }

    }
}
```


### 설명