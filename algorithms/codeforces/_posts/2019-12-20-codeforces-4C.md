---
layout: base
title: 코드포스 4C (codeforces 4C)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 4C (codeforces 4C) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/4C
tags:
  - codeforces
  - codeforces 4C
  - 코드포스
  - 코드포스 4C
  - Problem - 4C - Codeforces
---

## 코드포스 4C (codeforces 4C)
Problem - 4C - Codeforces

{% assign title = 'Codeforces 4C' %}
{% assign description = '코드포스 4C (codeforces 4C) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/4/C' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.HashMap;

public class CF4C {
    static int n;
    static String in;
    static HashMap<String, Integer> hm = new HashMap<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < n; i++) {
            in = br.readLine();
            if (hm.containsKey(in)) {
                sb.append(in + hm.get(in) + "\n");
                hm.put(in, hm.get(in) + 1);
            }else {
                hm.put(in, 1);sb.append("OK\n");
            }
        }
        bw.write(sb.toString());
        bw.flush();bw.close();
    }
}
```


### 설명