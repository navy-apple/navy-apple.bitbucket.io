---
layout: base
title: 코드포스 12A (codeforces 12A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 12A (codeforces 12A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/12A
tags:
  - codeforces
  - codeforces 12A
  - 코드포스
  - 코드포스 12A
  - Problem - 12A - Codeforces
---

## 코드포스 12A (codeforces 12A)
Problem - 12A - Codeforces

{% assign title = 'Codeforces 12A' %}
{% assign description = '코드포스 12A (codeforces 12A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/12/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF12A {
    static char[][] table = new char[3][3];
    static String ins;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        for (int i = 0; i < 3; i++) {
            ins = br.readLine();
            for (int j = 0; j < 3; j++) {
                table[i][j] = ins.charAt(j);
            }
        }
        boolean isSymmetric = true;
        for (int i = 0; i < 3; i++) {
            if (table[0][i] != table[2][2 - i]) {
                isSymmetric = false; break;
            }
        }
        if (table[1][0] != table[1][2]) isSymmetric = false;
        bw.write(((isSymmetric) ? "YES" : "NO") + "\n");
        bw.flush();bw.close();

    }
}
```


### 설명