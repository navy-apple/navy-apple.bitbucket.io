---
layout: base
title: 코드포스 58A (codeforces 58A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 58A (codeforces 58A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/58A
tags:
  - codeforces
  - codeforces 58A
  - 코드포스
  - 코드포스 58A
  - Problem - 58A - Codeforces
---

## 코드포스 58A (codeforces 58A)
Problem - 58A - Codeforces

{% assign title = 'Codeforces 58A' %}
{% assign description = '코드포스 58A (codeforces 58A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/58/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF58A {
    static String in;
    static byte[] hello = new byte[5];
    static boolean flag = true;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();
        for (int i = 0; i < in.length(); i++) {
            switch (in.charAt(i)) {
                case 'h': hello[0] = 1; break;
                case 'e': if (hello[0] == 1) hello[1] = 1; break;
                case 'l':
                    if (hello[1] == 1) {
                        if (hello[2] == 1) hello[3] = 1;
                        else hello[2] = 1;
                    }break;
                case 'o': if (hello[3] == 1) hello[4] = 1;break;
            }
        }
        for (int i = 0; i < 5; i++) {
            if (hello[i] != 1) { flag = false;break; }
        }
        bw.write(((flag) ? "YES" : "NO") + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명