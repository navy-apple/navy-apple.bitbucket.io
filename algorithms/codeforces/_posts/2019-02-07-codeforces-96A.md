---
layout: base
title: 코드포스 96A (codeforces 96A)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 96A (codeforces 96A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/96A
tags:
  - codeforces
  - codeforces 96A
  - 코드포스
  - 코드포스 96A
  - Problem - 96A - Codeforces
---

## 코드포스 96A (codeforces 96A)
Problem - 96A - Codeforces

{% assign title = 'Codeforces 96A' %}
{% assign description = '코드포스 96A (codeforces 96A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/96/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF96A {
    static String in;
    static int count = 0, pre;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();pre = in.charAt(0);
        for (int i = 1; i < in.length(); i++) {
            count = (in.charAt(i) == pre) ? count + 1 : 0;
            pre = in.charAt(i);
            if (count == 6) break;
        }
        bw.write((count == 6) ? "YES\n" : "NO\n");
        bw.flush();bw.close();
    }
}
```


### 설명