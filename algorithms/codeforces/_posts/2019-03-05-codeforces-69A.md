---
layout: base
title: 코드포스 69A (codeforces 69A)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 69A (codeforces 69A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/69A
tags:
  - codeforces
  - codeforces 69A
  - 코드포스
  - 코드포스 69A
  - Problem - 69A - Codeforces
---

## 코드포스 69A (codeforces 69A)
Problem - 69A - Codeforces

{% assign title = 'Codeforces 69A' %}
{% assign description = '코드포스 69A (codeforces 69A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/69/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF69A {
    static int n, x = 0, y = 0, z = 0;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            ins = br.readLine().split(" ");
            x += Integer.parseInt(ins[0]);
            y += Integer.parseInt(ins[1]);
            z += Integer.parseInt(ins[2]);
        }
        bw.write((x*x + y*y + z*z > 0) ? "NO\n" : "YES\n");
        bw.flush();bw.close();
    }
}
```


### 설명