---
layout: base
title: 코드포스 5A (codeforces 5A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 5A (codeforces 5A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/5A
tags:
  - codeforces
  - codeforces 5A
  - 코드포스
  - 코드포스 5A
  - Problem - 5A - Codeforces
---

## 코드포스 5A (codeforces 5A)
Problem - 5A - Codeforces

{% assign title = 'Codeforces 5A' %}
{% assign description = '코드포스 5A (codeforces 5A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/5/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF5A {
    static String in;
    static int m = 0, l = 0;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        while ((in = br.readLine()) != null){
            if (in.charAt(0) == '+' || in.charAt(0) == '-') {
                m += (in.charAt(0) == '+') ? 1 : -1;
                if (m < 0) m = 0;
            }else {
                String massage = in.substring(in.indexOf(':') + 1, in.length());
                l += massage.length() * m;
            }
        }
        bw.write(l + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명