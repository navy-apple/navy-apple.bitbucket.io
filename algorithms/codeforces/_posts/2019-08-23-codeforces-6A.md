---
layout: base
title: 코드포스 6A (codeforces 6A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 6A (codeforces 6A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/6A
tags:
  - codeforces
  - codeforces 6A
  - 코드포스
  - 코드포스 6A
  - Problem - 6A - Codeforces
---

## 코드포스 6A (codeforces 6A)
Problem - 6A - Codeforces

{% assign title = 'Codeforces 6A' %}
{% assign description = '코드포스 6A (codeforces 6A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/6/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class CF6A {
    static int[] s = new int[4];
    static String ins[];
    static boolean isTriangle = false, isSegment = false;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        for (int i = 0; i < 4; i++) {
            s[i] = Integer.parseInt(ins[i]);
        }
        for (int i = 0; i < 2; i++) {
            for(int j = i + 1; j < 3; j++) {
                for (int k = j + 1; k < 4; k++) {
                    triangle(s[i], s[j], s[k]);
                }
            }
        }
        String s = "";
        if (isTriangle) s = "TRIANGLE";
        else if (isSegment) s = "SEGMENT";
        else s = "IMPOSSIBLE";
        bw.write(s + "\n");
        bw.flush();bw.close();
    }
    public static void triangle(int a, int b, int c) {
        int[] t = {a, b, c};
        Arrays.sort(t);
        if (t[2] < t[0] + t[1]) isTriangle = true;
        if (t[2] == t[0] + t[1]) isSegment = true;
    }
}
```


### 설명