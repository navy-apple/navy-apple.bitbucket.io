---
layout: base
title: 코드포스 4A (codeforces 4A)
image: /assets/img/blog/codeforces.png
description: >
  코드포스 4A (codeforces 4A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/4A
tags:
  - codeforces
  - codeforces 4A
  - 코드포스
  - 코드포스 4A
  - Problem - 4A - Codeforces
---

## 코드포스 4A (codeforces 4A)
Problem - 4A - Codeforces

{% assign title = 'Codeforces 4A' %}
{% assign description = '코드포스 4A (codeforces 4A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/4/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF4A {
    static int w;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        w = Integer.parseInt(br.readLine());
        bw.write((w % 2 == 0 && w > 2) ? "YES\n" : "NO\n");
        bw.flush();bw.close();
    }
}
```


### 설명