---
layout: base
title: 코드포스 8A (codeforces 8A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 8A (codeforces 8A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/8A
tags:
  - codeforces
  - codeforces 8A
  - 코드포스
  - 코드포스 8A
  - Problem - 8A - Codeforces
---

## 코드포스 8A (codeforces 8A)
Problem - 8A - Codeforces

{% assign title = 'Codeforces 8A' %}
{% assign description = '코드포스 8A (codeforces 8A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/8/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF8A {
    static String in, s1, s2;
    static boolean isForward = false, isBackward = false;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();s1 = br.readLine();
        s2 = br.readLine();
        int idx = isSequence(in, s1);
        if (idx > 0 && idx < in.length()) {
            String ns1 = in.substring(idx, in.length());
            isForward = (isSequence(ns1, s2) > -1);
        }
        idx = isSequence(reverse(in), s1);
        if (idx > 0 && idx < in.length()) {
            String ns2 = reverse(in).substring(idx, in.length());
            isBackward = (isSequence(ns2, s2) > -1);
        }
        String ans = "";
        if (isForward && isBackward) ans = "both";
        else {
            if (isForward) ans = "forward";
            else if (isBackward) ans = "backward";
            else ans = "fantasy";
        }
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }

    public static int isSequence(String all, String sub) {
        int idx = all.indexOf(sub);
        idx = (idx < 0) ? -1 : idx + sub.length();
        return idx;
    }

    public static String reverse(String s) {
        return new StringBuffer(s).reverse().toString();
    }
}
```


### 설명