---
layout: base
title: 코드포스 580A (codeforces 580A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 580A (codeforces 580A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/580A
tags:
  - codeforces
  - codeforces 580A
  - 코드포스
  - 코드포스 580A
  - Problem - 580A - Codeforces
---

## 코드포스 580A (codeforces 580A)
Problem - 580A - Codeforces

{% assign title = 'Codeforces 580A' %}
{% assign description = '코드포스 580A (codeforces 580A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/580/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF580A {
    static int n;
    static int[] a = new int[100002];
    static String[] ins;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        ins = br.readLine().split(" ");
        for (int i = 1; i <= n; i++) {
            a[i] = Integer.parseInt(ins[i - 1]);
        }
        int max = 1, i = 1, count, cur = a[1];
        while (i != n) {
            count = 1;
            for (int j = i + 1; j <= n; j++) {
                if (j == n) i = n;
                if (cur > a[j]) { i = j; cur = a[j];break; }
                else {cur = a[j];count++;}
            }
            max = Math.max(max, count);
        }
        bw.write(max + "\n");bw.flush();
        bw.close();
    }
}
```


### 설명