---
layout: base
title: 코드포스 2A (codeforces 2A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 2A (codeforces 2A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/2A
tags:
  - codeforces
  - codeforces 2A
  - 코드포스
  - 코드포스 2A
  - Problem - 2A - Codeforces
---

## 코드포스 2A (codeforces 2A)
Problem - 2A - Codeforces

{% assign title = 'Codeforces 2A' %}
{% assign description = '코드포스 2A (codeforces 2A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/2/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Collections;
import java.util.HashMap;

public class CF2A {
    static int N, maxScore = 0;
    static HashMap<String, Integer> hm = new HashMap<>();
    static String in, ins[];
    static String winner;
    static String[] n = new String[1001];
    static int[] s = new int[1001];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();
        N = Integer.parseInt(in);
        for (int i = 0; i < N; i++) {
            ins = br.readLine().split(" ");
            n[i] = ins[0];s[i] = Integer.parseInt(ins[1]);
            s[i] += (hm.containsKey(ins[0])) ? hm.get(ins[0]) : 0;
            hm.put(n[i], s[i]);
        }
        maxScore = Collections.max(hm.values());
        for (int i = 0; i < N; i++) {
            if (hm.get(n[i]) == maxScore && s[i] >= maxScore) {
                winner = n[i]; break;
            }
        }
        bw.write(winner + "\n");
        bw.flush();bw.close();
    }

}
```


### 설명