---
layout: base
title: 코드포스 82A (codeforces 82A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 82A (codeforces 82A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/82A
tags:
  - codeforces
  - codeforces 82A
  - 코드포스
  - 코드포스 82A
  - Problem - 82A - Codeforces
---

## 코드포스 82A (codeforces 82A)
Problem - 82A - Codeforces

{% assign title = 'Codeforces 82A' %}
{% assign description = '코드포스 82A (codeforces 82A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/82/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.ArrayList;

public class CF82A {
    static int n, idx = -1, t;
    static String s[] = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};
    static ArrayList<Long> l = new ArrayList<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());init();
        for (int i = 1; i < l.size(); i++) {
            if (n <= l.get(i)) {
                idx = i - 1;break;
            }
        }
        n -= l.get(idx);t = 1 << (idx - 1);
        int count = 0;
        while (n > t) {
           n -= t;
           count++;
        }

        bw.write(s[count] + "\n");
        bw.flush();bw.close();
    }
    public static void init() {
        l.add((long)0);l.add((long)0);l.add((long)5);
        while (l.get(l.size() - 1) < 10e9) {
            l.add((l.get(l.size() - 1) - (l.get(l.size() - 2)) << 1) + l.get(l.size() - 1));
        }
    }
}
```


### 설명