---
layout: base
title: 코드포스 263A (codeforces 263A)
published: false
image: /assets/img/blog/codeforces.png
description: >
  코드포스 263A (codeforces 263A) 문제 풀이 및 코드 (description and code)
author: navy apple
comments: true
toc: true
permalink: /algorithms/codeforces/263A
tags:
  - codeforces
  - codeforces 263A
  - 코드포스
  - 코드포스 263A
  - Problem - 263A - Codeforces
---

## 코드포스 263A (codeforces 263A)
Problem - 263A - Codeforces

{% assign title = 'Codeforces 263A' %}
{% assign description = '코드포스 263A (codeforces 263A) 문제 풀이 및 코드 (description and code)' %}
{% assign url = 'https://codeforces.com/problemset/problem/263/A' %}
{% assign bg_img_url = '/assets/img/blog/codeforces.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class CF263A {
    static int x, y, ans = 0;
    static int[][] map = new int[6][6];
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        for (int i = 1; i < 6; i++) {
            ins = br.readLine().split(" ");
            for (int j = 1; j < 6; j++) {
                if (ins[j - 1].equals("1")) {
                    x = i; y = j;
                }
            }
        }
        ans += (Math.abs(x - 3) + Math.abs(y - 3));
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명