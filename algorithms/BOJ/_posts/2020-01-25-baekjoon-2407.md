---
layout: base
title: (BOJ)백준 2407 (baekjoon 2407)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 2407 - 2407번: 조합
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/2407
tags:
  - BOJ
  - BOJ 2407
  - 백준
  - 백준 2407
  - 2407번 조합
---

## [BOJ]백준 2407번: 조합 (baekjoon 2407)
2407번: 조합

{% assign title = '[BOJ] 백준 (2407)' %}
{% assign description = '백준 2407 - 2407번: 조합' %}
{% assign url = 'https://www.acmicpc.net/problem/2407' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.math.BigInteger;

public class B2407 {
    static int N, M;
    static String in;
    static BigInteger[] factorial = new BigInteger[101];
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        in = br.readLine();
        N = Integer.parseInt(in.split(" ")[0]);
        M = Integer.parseInt(in.split(" ")[1]);
        factorialInit();
        BigInteger ans = factorial[N].divide(factorial[M].multiply(factorial[N - M]));
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }

    public static void factorialInit() {
        factorial[1] = BigInteger.ONE;
        for (int i = 2; i < 101; i++) {
            factorial[i] = factorial[i - 1].multiply(BigInteger.valueOf(i));
        }
    }

}
```


### 설명