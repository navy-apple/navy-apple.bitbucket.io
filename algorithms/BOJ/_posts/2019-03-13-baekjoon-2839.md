---
layout: base
title: (BOJ)백준 2839 (baekjoon 2839)
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 2839 - 2839번: 설탕 배달
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/2839
tags:
  - BOJ
  - BOJ 2839
  - 백준
  - 백준 2839
  - 2839번 설탕 배달
---

## [BOJ]백준 2839번: 설탕 배달 (baekjoon 2839)
2839번: 설탕 배달

{% assign title = '[BOJ] 백준 (2839)' %}
{% assign description = '백준 2839 - 2839번: 설탕 배달' %}
{% assign url = 'https://www.acmicpc.net/problem/2839' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
//이런 간단한 문제도 어렵게 생각하는 경우가 있음
public class B2839 {
    static int N, remain, min = Integer.MAX_VALUE;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        N = Integer.parseInt(br.readLine());
        for (int i = 0; i <= N/5; i++) {
            remain = N - i * 5;
            if (remain % 3 == 0) min = Math.min(min, i + (remain/3));
        }
        bw.write((min == Integer.MAX_VALUE) ? "-1\n" : min + "\n");
        bw.flush();bw.close();
    }

}
```


### 설명