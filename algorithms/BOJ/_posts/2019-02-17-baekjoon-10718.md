---
layout: base
title: (BOJ)백준 10718 (baekjoon 10718)
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 10718 - 10718번: We love kriii
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/10718
tags:
  - BOJ
  - BOJ 10718
  - 백준
  - 백준 10718
  - 10718번 We love kriii
---

## [BOJ]백준 10718번: We love kriii (baekjoon 10718)
10718번: We love kriii

{% assign title = '[BOJ] 백준 (10718)' %}
{% assign description = '백준 10718 - 10718번: We love kriii' %}
{% assign url = 'https://www.acmicpc.net/problem/10718' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class B10718 {
    public static void main(String[] args) throws IOException{
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        bw.write("강한친구 대한육군\n" +
                "강한친구 대한육군\n");bw.flush();bw.close();
    }
}
```


### 설명