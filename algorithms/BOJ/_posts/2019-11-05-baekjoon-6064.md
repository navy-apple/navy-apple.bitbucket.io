---
layout: base
title: (BOJ)백준 6064 (baekjoon 6064)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 6064 - 6064번: 카잉 달력
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/6064
tags:
  - BOJ
  - BOJ 6064
  - 백준
  - 백준 6064
  - 6064번 카잉 달력
---

## [BOJ]백준 6064번: 카잉 달력 (baekjoon 6064)
6064번: 카잉 달력

{% assign title = '[BOJ] 백준 (6064)' %}
{% assign description = '백준 6064 - 6064번: 카잉 달력' %}
{% assign url = 'https://www.acmicpc.net/problem/6064' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B6064 {
    static int T, N, M, x, y;
    static String in;
    static boolean isDay;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        T = Integer.parseInt(br.readLine());
        for (int i = 0; i < T; i++) {
            in = br.readLine();isDay = false;
            M = Integer.parseInt(in.split(" ")[0]);
            N = Integer.parseInt(in.split(" ")[1]);
            x = Integer.parseInt(in.split(" ")[2]);
            y = Integer.parseInt(in.split(" ")[3]);
            int result = -1;
            for(int j = 0; j < N; j++) {
                if((j * M + x - 1) % N + 1 == y) {
                    result = j * M + x;
                    break;
                }
            }
            bw.write(result + "\n");
            bw.flush();
        }
        bw.close();
    }

}
```


### 설명