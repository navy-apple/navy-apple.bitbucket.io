---
layout: base
title: (BOJ)백준 10809 (baekjoon 10809)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 10809 - 10809번: 알파벳 찾기
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/10809
tags:
  - BOJ
  - BOJ 10809
  - 백준
  - 백준 10809
  - 10809번 알파벳 찾기
---

## [BOJ]백준 10809번: 알파벳 찾기 (baekjoon 10809)
10809번: 알파벳 찾기

{% assign title = '[BOJ] 백준 (10809)' %}
{% assign description = '백준 10809 - 10809번: 알파벳 찾기' %}
{% assign url = 'https://www.acmicpc.net/problem/10809' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class B10809 {
    static int[] alphabet = new int[26];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        String in = br.readLine();
        Arrays.fill(alphabet, -1);
        for (int i = 0; i < in.length() ; i++) {
            if (alphabet[in.charAt(i) - 97] == -1)
                alphabet[in.charAt(i) - 97] = i;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < alphabet.length; i++) {
            sb.append(alphabet[i] + " ");
        }
        bw.write(sb.toString().substring(0, sb.length() - 1) + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명