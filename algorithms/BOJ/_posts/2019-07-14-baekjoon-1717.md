---
layout: base
title: (BOJ)백준 1717 (baekjoon 1717)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1717 - 1717번: 집합의 표현
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1717
tags:
  - BOJ
  - BOJ 1717
  - 백준
  - 백준 1717
  - 1717번 집합의 표현
---

## [BOJ]백준 1717번: 집합의 표현 (baekjoon 1717)
1717번: 집합의 표현

{% assign title = '[BOJ] 백준 (1717)' %}
{% assign description = '백준 1717 - 1717번: 집합의 표현' %}
{% assign url = 'https://www.acmicpc.net/problem/1717' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class B1717 {
    static int n, m, a, b, p[] = new int[1000001];
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        n = Integer.parseInt(ins[0]);
        m = Integer.parseInt(ins[1]);
        Arrays.fill(p, -1);
        StringBuilder sb = new StringBuilder();
        while (m-- != 0) {
            ins = br.readLine().split(" ");
            a = Integer.parseInt(ins[1]);
            b = Integer.parseInt(ins[2]);
            if (ins[0].equals("0")) union(a, b);
            else {
                int pa = find(a), pb = find(b);
                sb.append(((pa != -1 && pb != -1 && (pa == pb)) ? "YES\n" : "NO\n"));
            }
        }
        bw.write(sb.toString());
        bw.flush();bw.close();
    }

    public static int find(int n) {
        if (p[n] < 0) return n;
        p[n] = find(p[n]);
        return p[n];
    }
    public static void union(int a, int b) {
        a = find(a);
        b = find(b);
        if (a == b) return;
        p[b] = a;
    }

}
```


### 설명