---
layout: base
title: (BOJ)백준 1003 (baekjoon 1003)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1003 - 1003번: 피보나치 함수
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1003
tags:
  - BOJ
  - BOJ 1003
  - 백준
  - 백준 1003
  - 1003번 피보나치 함수
---

## [BOJ]백준 1003번: 피보나치 함수 (baekjoon 1003)
1003번: 피보나치 함수

{% assign title = '[BOJ] 백준 (1003)' %}
{% assign description = '백준 1003 - 1003번: 피보나치 함수' %}
{% assign url = 'https://www.acmicpc.net/problem/1003' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B1003 {
    static int T, n, one, zero;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        T = Integer.parseInt(br.readLine());
        for (int i = 0; i < T; i++) {
            n = Integer.parseInt(br.readLine());
            one = 0;zero = 0;
            fibonacci(n);
            bw.write(zero + " " + one + "\n");
            bw.flush();
        }
        bw.close();
    }

    public static int fibonacci(int n) {
        if (n == 0 || n == 1) {
            one += (n == 1) ? 1 : 0;
            zero += (n == 0) ? 1 : 0;
            return 1;
        }else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}
```


### 설명