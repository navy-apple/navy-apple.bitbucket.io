---
layout: base
title: (BOJ)백준 1629 (baekjoon 1629)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1629 - 1629번: 곱셈
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1629
tags:
  - BOJ
  - BOJ 1629
  - 백준
  - 백준 1629
  - 1629번 곱셈
---

## [BOJ]백준 1629번: 곱셈 (baekjoon 1629)
1629번: 곱셈

{% assign title = '[BOJ] 백준 (1629)' %}
{% assign description = '백준 1629 - 1629번: 곱셈' %}
{% assign url = 'https://www.acmicpc.net/problem/1629' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B1629 {
    static int A, B, C;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        A = Integer.parseInt(ins[0]);
        B = Integer.parseInt(ins[1]);
        C = Integer.parseInt(ins[2]);
        bw.write(pow(B) + "\n");
        bw.flush();bw.close();
    }

    public static long pow(int b) {
        if (b == 1) return A % C;
        long t = pow(b >> 1);
        long ans = t * t;
        ans %= C;
        if (b % 2 != 0) {
            ans *= A;ans %= C;
        }
        return ans;
    }
}
```


### 설명