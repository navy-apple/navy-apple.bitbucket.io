---
layout: base
title: (BOJ)백준 1546 (baekjoon 1546)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1546 - 1546번: 평균
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1546
tags:
  - BOJ
  - BOJ 1546
  - 백준
  - 백준 1546
  - 1546번 평균
---

## [BOJ]백준 1546번: 평균 (baekjoon 1546)
1546번: 평균

{% assign title = '[BOJ] 백준 (1546)' %}
{% assign description = '백준 1546 - 1546번: 평균' %}
{% assign url = 'https://www.acmicpc.net/problem/1546' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B1546 {
    static int N;
    static String in, ins[];
    static float[] scores = new float[1001];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        N = Integer.parseInt(br.readLine());
        in = br.readLine();ins = in.split(" ");
        for (int i = 0; i < ins.length; i++) {
            scores[i] = Float.parseFloat(ins[i]);
        }
        float max = 0, mean = 0;
        for (int i = 0; i < N; i++) {
            max = Math.max(max, scores[i]);
        }
        for (int i = 0; i < N; i++) {
            scores[i] /= max;scores[i] *= 100.;
            mean += scores[i];
        }
        mean /= (float)N;
        bw.write(mean + "\n");bw.flush();
        bw.close();

    }
}
```


### 설명