---
layout: base
title: (BOJ)백준 11728 (baekjoon 11728)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 11728 - 11728번: 배열 합치기
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/11728
tags:
  - BOJ
  - BOJ 11728
  - 백준
  - 백준 11728
  - 11728번 배열 합치기
---

## [BOJ]백준 11728번: 배열 합치기 (baekjoon 11728)
11728번: 배열 합치기

{% assign title = '[BOJ] 백준 (11728)' %}
{% assign description = '백준 11728 - 11728번: 배열 합치기' %}
{% assign url = 'https://www.acmicpc.net/problem/11728' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.util.Scanner;

public class BaekJoon11728 {
    static int n, m, a, b, indexA, indexB;
    static int[] A, B, Answer;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();m = sc.nextInt();
        indexA = 0; indexB = 0;
        A = new int[n];B = new int[m];
        Answer = new int[n + m];
        for (int i = 0; i < n; i++) {
            A[i] = sc.nextInt();
        }
        for (int i = 0; i < m; i++) {
            B[i] = sc.nextInt();
        }
        for (int i = 0; i < Answer.length; i++) {
            a = (indexA < n) ? A[indexA] : 1000000007;
            b = (indexB < m) ? B[indexB] : 1000000007;
            Answer[i] = (a < b) ? a : b;
            if (a < b) indexA++;else indexB++;
            System.out.print(Answer[i]);
            if (i < Answer.length - 1) System.out.print(" ");
        }

    }
}
```


### 설명