---
layout: base
title: (BOJ)백준 1405 (baekjoon 1405)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1405 - 1405번: 미친 로봇
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1405
tags:
  - BOJ
  - BOJ 1405
  - 백준
  - 백준 1405
  - 1405번 미친 로봇
---

## [BOJ]백준 1405번: 미친 로봇 (baekjoon 1405)
1405번: 미친 로봇

{% assign title = '[BOJ] 백준 (1405)' %}
{% assign description = '백준 1405 - 1405번: 미친 로봇' %}
{% assign url = 'https://www.acmicpc.net/problem/1405' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B1405 {
    static int n, dx[] = {0, 0, 1, -1}, dy[] = {-1, 1, 0, 0};
    static double pr[] = new double[4], ans = 0;
    static String ins[];
    static boolean[][] visited = new boolean[30][30];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        ins = br.readLine().split(" ");
        n = Integer.parseInt(ins[0]);
        for (int i = 0; i < 4; i++) {
            pr[i] = Double.parseDouble(ins[i + 1]) / 100.;
        }
        dfs(0, new Point(15, 15), 1);
        bw.write(ans + "\n");
        bw.flush();bw.close();
    }

    public static void dfs(int idx, Point p, double prob) {
        if (idx == n) {
            ans += prob;
            return;
        }
        visited[p.x][p.y] = true;
        for (int i = 0; i < 4; i++) {
            int nx = p.x + dx[i], ny = p.y + dy[i];
            if (!visited[nx][ny]) dfs(idx + 1, new Point(nx, ny), pr[i] * prob);
        }
        visited[p.x][p.y] = false;
    }

    
    private static class Point {
        int x, y;
        Point(int x, int y) {
            this.x = x; this.y = y;
        }
    }
}
```


### 설명