---
layout: base
title: (BOJ)백준 11050 (baekjoon 11050)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 11050 - 11050번: 이항 계수 1
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/11050
tags:
  - BOJ
  - BOJ 11050
  - 백준
  - 백준 11050
  - 11050번 이항 계수 1
---

## [BOJ]백준 11050번: 이항 계수 1 (baekjoon 11050)
11050번: 이항 계수 1

{% assign title = '[BOJ] 백준 (11050)' %}
{% assign description = '백준 11050 - 11050번: 이항 계수 1' %}
{% assign url = 'https://www.acmicpc.net/problem/11050' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B11050 {
    final static int MOD = 1000000007;
    static int[] factorial = new int[200005];
    static int[] inverseFactorial = new int[200005];
    static int N, M;
    static String in;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        factorialInit();
        in = br.readLine();
        N = Integer.parseInt(in.split(" ")[0]);
        M = Integer.parseInt(in.split(" ")[1]);
        bw.write(combination(N, M) + "\n");
        bw.flush();bw.close();
    }
    private static void factorialInit() {
        factorial[0] = inverseFactorial[0] = 1;
        long t = 1;
        for (int i = 1; i < 200005; i++) {
            t = (t * i) % MOD;
            factorial[i] = (int)t;
            inverseFactorial[i] = (int)exp(t, MOD - 2);
        }
    }
    private static int combination(int a, int b){
        long e= factorial[a];
        e = (e*inverseFactorial[b]) % MOD;
        e = (e*inverseFactorial[a-b]) % MOD;
        return (int)e;
    }
    private static long exp(long x, int m) {
        if(m==1) return x;
        long t = exp(x,m/2);
        long result = t;
        result = (result * t) % MOD;
        if(m % 2 == 1 ){
            result = (result * x) % MOD;
        }
        return (int) result;
    }
}
```


### 설명