---
layout: base
title: (BOJ)백준 2675 (baekjoon 2675)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 2675 - 2675번: 문자열 반복
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/2675
tags:
  - BOJ
  - BOJ 2675
  - 백준
  - 백준 2675
  - 2675번 문자열 반복
---

## [BOJ]백준 2675번: 문자열 반복 (baekjoon 2675)
2675번: 문자열 반복

{% assign title = '[BOJ] 백준 (2675)' %}
{% assign description = '백준 2675 - 2675번: 문자열 반복' %}
{% assign url = 'https://www.acmicpc.net/problem/2675' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class B2675 {
    static int N, R;
    static String str, in;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        N = Integer.parseInt(br.readLine());
        for (int i = 0; i < N; i++) {
            in = br.readLine();
            R = Integer.parseInt(in.split(" ")[0]);
            str = in.split(" ")[1];
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < str.length(); j++) {
                for (int k = 0; k < R; k++) {
                    sb.append(str.charAt(j));
                }
            }
            bw.write(sb.toString() + "\n");
            bw.flush();
        }
        bw.close();
    }
}
```


### 설명