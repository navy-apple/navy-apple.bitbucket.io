---
layout: base
title: (BOJ)백준 1182 (baekjoon 1182)
published: false
image: /assets/img/blog/boj.png
description: >
  (BOJ)백준 1182 - 1182번: 부분수열의 합
author: navy apple
comments: true
toc: true
permalink: /algorithms/BOJ/1182
tags:
  - BOJ
  - BOJ 1182
  - 백준
  - 백준 1182
  - 1182번 부분수열의 합
---

## [BOJ]백준 1182번: 부분수열의 합 (baekjoon 1182)
1182번: 부분수열의 합

{% assign title = '[BOJ] 백준 (1182)' %}
{% assign description = '백준 1182 - 1182번: 부분수열의 합' %}
{% assign url = 'https://www.acmicpc.net/problem/1182' %}
{% assign bg_img_url = '/assets/img/blog/boj.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by im501 on 2017-08-24.
 */
public class BaekJoon1182 {
    static int N, S, Answer, numbers[], subSet[], count, index, o;
    static Stack<Integer> stack = new Stack<>();
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();S = sc.nextInt();
        numbers = new int[N];Answer = 0;count = 0;
        for (int i = 0; i < N; i++) {
            numbers[i] = sc.nextInt();
        }

        for (int i = 1; i < N + 1; i++) { // i개로 이루어진 부분 집합.
            Answer = 0; subSet = new int[i];index = 0;

        }
        System.out.println(count);
    }

}
```


### 설명