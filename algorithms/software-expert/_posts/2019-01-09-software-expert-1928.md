---
layout: base
title: 소프트웨어 익스퍼트 1928 (software expert 1928)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1928 (software expert 1928)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1928
tags:
  - software expert academy
  - software expert 1928
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1928
---

## 소프트웨어 익스퍼트 1928 (software expert 1928)
소프트웨어 익스퍼트 1928

{% assign title = 'Software Expert Academy 1928' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1928 (Software Expert Academy 1928)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1928 {
    static int t;
    static String in;
    static char[] c;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        t = Integer.parseInt(br.readLine());
        for (int i = 1; i <= t; i++) {
            in = br.readLine();
            c = in.toCharArray();
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < c.length; j++) {
                int v;
                if (c[j] > 64 && c[j] < 91) v = c[j] - 65;
                else if(c[j] > 96 && c[j] < 123) v = c[j] - 71;
                else if (c[j] > 47 && c[j] < 58) v = c[j] + 4;
                else v = c[j] + 19;
                String s = Integer.toBinaryString(v);
                while (s.length() < 6) s = "0" + s;
                sb.append(s);
            }
            String byteString;
            StringBuffer ans = new StringBuffer();
            for (int j = 0; j < sb.length(); j+=8) {
                byteString = sb.toString().substring(j, j + 8);
                int dec = Integer.parseInt(byteString, 2);
                ans.append((char)dec);
            }
            bw.write("#" + i + " " + ans + "\n");
            bw.flush();
        }
        bw.close();
    }
}
```


### 설명