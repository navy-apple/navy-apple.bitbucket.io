---
layout: base
title: 소프트웨어 익스퍼트 1259 (software expert 1259)
published: false
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1259 (software expert 1259)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1259
tags:
  - software expert academy
  - software expert 1259
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1259
---

## 소프트웨어 익스퍼트 1259 (software expert 1259)
소프트웨어 익스퍼트 1259

{% assign title = 'Software Expert Academy 1259' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1259 (Software Expert Academy 1259)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;

public class SWE1259 {
    static int t, T, n;
    static String ins[];
    static Nail[] ns = new Nail[200001];
    static int[][] dp = new int[200001][31];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        t = T = Integer.parseInt(br.readLine());
        while (T-- != 0) {
            n = Integer.parseInt(br.readLine());
            ins = br.readLine().split(" ");
            for (int i = 0; i < (n << 1); i += 2) {
                ns[i >> 1] = new Nail(Integer.parseInt(ins[i]), Integer.parseInt(ins[i + 1]));
            }
            for (int i = 0; i < 200001; i++) Arrays.fill(dp[i], -1);
            bw.write("#" + (t - T) + " "  + "\n");
            bw.flush();bw.close();
        }
    }
    private static int dfs(int idx, int next) {
        if (idx < 0) return 0;
        if (idx == n) {
          return 0;
        } else if (dp[idx][next] != -1) return dp[idx][next];
        else {
            dp[idx][next] = Math.max(dp[idx][next], dfs(idx - 1, ns[idx].prev) + 1);
            return dp[idx][next];
        }
    }

    private static class Nail {
        int prev, next;
        Nail(int prev, int next) {
            this.prev = prev;this.next = next;
        }
    }
}
```


### 설명