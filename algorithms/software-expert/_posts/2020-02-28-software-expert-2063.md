---
layout: base
title: 소프트웨어 익스퍼트 2063 (software expert 2063)
published: false
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 2063 (software expert 2063)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/2063
tags:
  - software expert academy
  - software expert 2063
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 2063
---

## 소프트웨어 익스퍼트 2063 (software expert 2063)
소프트웨어 익스퍼트 2063

{% assign title = 'Software Expert Academy 2063' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 2063 (Software Expert Academy 2063)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class SWE2063 {
    static int n, a[] = new int[200];
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        ins = br.readLine().split(" ");
        for (int i = 0; i < n; i++) a[i] = Integer.parseInt(ins[i]);
        Arrays.parallelSort(a, 0, n);
        bw.write(a[(n >> 1)] + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명