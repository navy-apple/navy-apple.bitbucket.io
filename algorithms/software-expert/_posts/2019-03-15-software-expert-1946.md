---
layout: base
title: 소프트웨어 익스퍼트 1946 (software expert 1946)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1946 (software expert 1946)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1946
tags:
  - software expert academy
  - software expert 1946
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1946
---

## 소프트웨어 익스퍼트 1946 (software expert 1946)
소프트웨어 익스퍼트 1946

{% assign title = 'Software Expert Academy 1946' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1946 (Software Expert Academy 1946)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1946 {
    static int t, T, n, k;
    static String ins[], c;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        T = t = Integer.parseInt(br.readLine());
        while (T-- != 0) {
            n = Integer.parseInt(br.readLine());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < n; i++) {
                ins = br.readLine().split(" ");
                c = ins[0]; k = Integer.parseInt(ins[1]);
                for (int j = 0; j < k; j++)  sb.append(c);

            }
            StringBuffer ans = new StringBuffer();
            for (int i = 0; i < sb.length(); i += 10) {
                ans.append(sb.substring(i, (i + 10 <= sb.length()) ? i + 10 : sb.length()) + "\n");
            }
            bw.write("#" + (t - T) + "\n" + ans.toString());bw.flush();
        }
        bw.close();
    }
}
```


### 설명