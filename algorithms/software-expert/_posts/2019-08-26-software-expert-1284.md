---
layout: base
title: 소프트웨어 익스퍼트 1284 (software expert 1284)
published: false
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1284 (software expert 1284)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1284
tags:
  - software expert academy
  - software expert 1284
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1284
---

## 소프트웨어 익스퍼트 1284 (software expert 1284)
소프트웨어 익스퍼트 1284

{% assign title = 'Software Expert Academy 1284' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1284 (Software Expert Academy 1284)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1284 {
    static int t, p, q, r, s, w, ans, a ,b;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        t = Integer.parseInt(br.readLine());
        for (int i = 1; i <= t; i++) {
            ins = br.readLine().split(" ");
            p = Integer.parseInt(ins[0]);q = Integer.parseInt(ins[1]);
            r = Integer.parseInt(ins[2]);s = Integer.parseInt(ins[3]);
            w = Integer.parseInt(ins[4]);
            a = w * p;b = (w > r) ?  q + (w - r) * s : q;
            bw.write("#" + i + " " + Math.min(a, b) + "\n");bw.flush();
        }
        bw.close();
    }
}
```


### 설명