---
layout: base
title: 소프트웨어 익스퍼트 1204 (software expert 1204)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1204 (software expert 1204)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1204
tags:
  - software expert academy
  - software expert 1204
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1204
---

## 소프트웨어 익스퍼트 1204 (software expert 1204)
소프트웨어 익스퍼트 1204

{% assign title = 'Software Expert Academy 1204' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1204 (Software Expert Academy 1204)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Arrays;

public class SWE1204 {
    static int t = 10 ,n;
    static long[] a = new long[101];
    static String ins[], s;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        while (t-- != 0) {
            n = Integer.parseInt(br.readLine());
            Arrays.fill(a, 0);
            ins = br.readLine().split(" ");
            for (int i = 0; i < ins.length; i++) {
                a[Integer.parseInt(ins[i])]++;
            }
            int maxIdx = -1;
            long max = -1;
            for (int i = 0; i < 101; i++) {
                if (max <= a[i]) {
                    max = a[i];maxIdx = i;
                }
            }
            bw.write("#" + n + " " + maxIdx + "\n");
            bw.flush();
        }
        bw.close();
    }
}
```


### 설명