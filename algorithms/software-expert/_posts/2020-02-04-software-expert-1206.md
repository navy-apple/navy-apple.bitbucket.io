---
layout: base
title: 소프트웨어 익스퍼트 1206 (software expert 1206)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1206 (software expert 1206)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1206
tags:
  - software expert academy
  - software expert 1206
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1206
---

## 소프트웨어 익스퍼트 1206 (software expert 1206)
소프트웨어 익스퍼트 1206

{% assign title = 'Software Expert Academy 1206' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1206 (Software Expert Academy 1206)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1206 {
    static int t = 10, n;
    static int[] a = new int[1005];
    static String[] ins;
    static int[] dy = {-2, -1, 1, 2};
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        while (t -- != 0) {
            n = Integer.parseInt(br.readLine());
            ins = br.readLine().split(" ");
            for (int i = 2; i < ins.length + 2; i++) {
                a[i] = Integer.parseInt(ins[i - 2]);
            }
            long ans = 0;
            for (int i = 2; i < n; i++) {
                int max = 0;
                for (int j = 0; j < 4; j++) {
                    int ni = i + dy[j];
                    max = Math.max(max, a[ni]);
                }
                ans += (a[i] > max) ? a[i] - max : 0;
            }
            bw.write("#" + (10 - t) + " " + ans + "\n");
            bw.flush();
        }
        bw.close();
    }
}
```


### 설명