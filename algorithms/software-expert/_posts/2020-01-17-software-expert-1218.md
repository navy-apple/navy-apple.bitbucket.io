---
layout: base
title: 소프트웨어 익스퍼트 1218 (software expert 1218)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1218 (software expert 1218)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1218
tags:
  - software expert academy
  - software expert 1218
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1218
---

## 소프트웨어 익스퍼트 1218 (software expert 1218)
소프트웨어 익스퍼트 1218

{% assign title = 'Software Expert Academy 1218' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1218 (Software Expert Academy 1218)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;
import java.util.Stack;

public class SWE1218 {
    static int t = 10, n;
    static String in;
    static boolean isVPS;
    static Stack<Character> s = new Stack<>();
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        while (t-- != 0) {
            n = Integer.parseInt(br.readLine());
            in = br.readLine();isVPS = true;
            for (int i = 0; i < in.length(); i++) {
                switch (in.charAt(i)) {
                    case '(':case '[':case '<':case '{': s.push(in.charAt(i));break;
                    case ')': if (s.peek() == '(') s.pop();else isVPS = false;break;
                    case ']': if (s.peek() == '[') s.pop();else isVPS = false;break;
                    case '>': if (s.peek() == '<') s.pop();else isVPS = false;break;
                    case '}': if (s.peek() == '{') s.pop();else isVPS = false;break;
                }
            }
            bw.write("#" + (10 - t) + " " + (isVPS && s.isEmpty() ? "1" : "0") + "\n");
            bw.flush();s.clear();
        }
        bw.close();
    }
}
```


### 설명