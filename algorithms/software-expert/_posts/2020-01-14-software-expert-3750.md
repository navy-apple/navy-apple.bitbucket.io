---
layout: base
title: 소프트웨어 익스퍼트 3750 (software expert 3750)
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 3750 (software expert 3750)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/3750
tags:
  - software expert academy
  - software expert 3750
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 3750
---

## 소프트웨어 익스퍼트 3750 (software expert 3750)
소프트웨어 익스퍼트 3750

{% assign title = 'Software Expert Academy 3750' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 3750 (Software Expert Academy 3750)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE3750 {
    static int t, T, sum;
    static String n;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        t = T = Integer.parseInt(br.readLine());
        StringBuilder sb = new StringBuilder();
        while (T-- != 0) {
            n = br.readLine();
            while (n.length() != 1) {
                sum = 0;
                for (int i = 0; i < n.length(); i++) {
                    sum += n.charAt(i) - 48;
                }
                n = sum + "";
            }
            sb.append("#");sb.append(t - T);sb.append(" ");
            sb.append(n);sb.append("\n");
        }
        bw.write(sb.toString());
        bw.flush();bw.close();
    }
}
```


### 설명