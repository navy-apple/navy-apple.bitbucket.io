---
layout: base
title: 소프트웨어 익스퍼트 1222 (software expert 1222)
published: false
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1222 (software expert 1222)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1222
tags:
  - software expert academy
  - software expert 1222
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1222
---

## 소프트웨어 익스퍼트 1222 (software expert 1222)
소프트웨어 익스퍼트 1222

{% assign title = 'Software Expert Academy 1222' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1222 (Software Expert Academy 1222)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1222 {
    static int n, t = 10;
    static String ins[];
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        while (t-- != 0) {
            n = Integer.parseInt(br.readLine());
            ins = br.readLine().split("\\+");
            int ans = 0;
            for (int i = 0; i < ins.length; i++) {
                ans += Integer.parseInt(ins[i]);
            }
            bw.write("#" + (10 - t) + " " + ans + "\n");
            bw.flush();
        }
        bw.close();
    }
}
```


### 설명