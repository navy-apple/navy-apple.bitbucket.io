---
layout: base
title: 소프트웨어 익스퍼트 1545 (software expert 1545)
published: false
image: /assets/img/blog/swe.png
description: >
  소프트웨어 익스퍼트 1545 (software expert 1545)
author: navy apple
comments: true
toc: true
permalink: /algorithms/software-expert/1545
tags:
  - software expert academy
  - software expert 1545
  - 소프트웨어 익스퍼트 아카데미
  - 소프트웨어 익스퍼트 1545
---

## 소프트웨어 익스퍼트 1545 (software expert 1545)
소프트웨어 익스퍼트 1545

{% assign title = 'Software Expert Academy 1545' %}
{% assign description = '소프트웨어 익스퍼트 아카데미 1545 (Software Expert Academy 1545)' %}
{% assign url = 'https://swexpertacademy.com' %}
{% assign bg_img_url = '/assets/img/blog/swe.png' %}

{% include base/components/link-box.html title=title description=description url=url bg_img_url=bg_img_url %}

### 코드
```java
import java.io.*;

public class SWE1545 {
    static int n;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        n = Integer.parseInt(br.readLine());
        StringBuffer sb = new StringBuffer();
        for (int i = n; i > -1; i--) {
            sb.append(i + " ");
        }
        bw.write(sb.toString().substring(0, sb.length() - 1) + "\n");
        bw.flush();bw.close();
    }
}
```


### 설명